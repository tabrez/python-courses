# %% import packages
import altair as alt
import pandas as pd
import plotnine as pn
import lets_plot as lp
from datasets import load_dataset

# %% load dataset and convert it to a pandas data frame
data = load_dataset('dair-ai/emotion')
print('data:', data)
data.set_format('pandas')


# %% plot label frequencies for three splits of the dataset: train, validation & test
def plot_label_frequencies(df):
  label_freqs_series = df['label'].value_counts()
  # print('type of label_freqs_series:', type(label_freqs_series))
  # print('type of label_freqs_df:', type(label_freqs_df))
  label_freqs_df = label_freqs_series.to_frame().reset_index()
  # print('label_freqs_df:\n', label_freqs_df)
  return alt.Chart(label_freqs_df).mark_bar().encode(x='label', y='count')


train_plot = plot_label_frequencies(data['train'][:])
validation_plot = plot_label_frequencies(data['validation'][:])
test_plot = plot_label_frequencies(data['test'][:])

train_plot | validation_plot | test_plot


# %% plot label frequencies using `plotnine`
def get_freq_df(data: pd.DataFrame, split: str) -> pd.DataFrame:
  s: pd.Series = data[split][:]['label'].value_counts()
  df: pd.DataFrame = s.to_frame().reset_index()
  df['split'] = split
  # print('df:\n', df.info())
  return df


df: pd.DataFrame = pd.concat(
  [
    get_freq_df(data, 'train'),
    get_freq_df(data, 'validation'),
    get_freq_df(data, 'test'),
  ]
)
df['split'] = df['split'].astype('category')
# print('assembled df:\n', df.info())
df['split'] = df['split'].cat.reorder_categories(['train', 'validation', 'test'])
# print('final df:\n', df.info)

# %%
(
  pn.ggplot(df, pn.aes(x='label', y='count'))
  + pn.geom_bar(stat='identity')
  + pn.facet_grid('. ~ split')
)

# %% plot label frequencies using `lets-plot`
lp.LetsPlot.setup_html()
(
  lp.ggplot(df, lp.aes(x='label', y='count'))
  + lp.geom_bar(stat='identity')
  + lp.facet_grid(x='split')
)
