#%% import packages
import mlflow
import numpy as np
import pandas as pd
from mlflow.models import infer_signature
from sklearn.datasets import load_diabetes
from sklearn.ensemble import RandomForestRegressor
from sklearn.metrics import mean_absolute_error, mean_squared_error, r2_score
from sklearn.model_selection import train_test_split

#%% mlflow autolog method
mlflow.autolog()

db = load_diabetes()
X_train, X_test, y_train, y_test = train_test_split(db.data, db.target)

rf = RandomForestRegressor(n_estimators=100, max_depth=6, max_features=3)
rf.fit(X_train, y_train)

predictions = rf.predict(X_test)
print(predictions)

#%% load saved model from disk and call predict again
## following folder can be found at ./mlruns/0/
logged_model = 'runs:/eeedc9649a4648169e4dd8cca2a79f5a/model'
loaded_model = mlflow.pyfunc.load_model(logged_model)
results = loaded_model.predict(pd.DataFrame(X_test))
print('results:', results)

#%% manually log using mlflow
def log_metrics(x, y, predictions, label):
  mse = mean_squared_error(y, predictions)
  rmse = np.sqrt(mse)
  mae = mean_absolute_error(y, predictions)
  r2 = r2_score(y, predictions)
  score = rf.score(x, y)

  mlflow.log_metric(f'{label}_mean_squared_error', mse)
  mlflow.log_metric(f'{label}_root_mean_squared_error', rmse)
  mlflow.log_metric(f'{label}_mean_absolute_error', mae)
  mlflow.log_metric(f'{label}_r2_score', r2)
  mlflow.log_metric(f'{label}_score', score)

with mlflow.start_run() as run:
  db = load_diabetes()
  X_train, X_test, y_train, y_test = train_test_split(db.data, db.target)
  print('type of X:', type(X_train), 'type of y_train:', type(y_train))
  print('shape of X_train:', X_train.shape, 'shape of y_train:', y_train)
  print('len of X:', len(X_train), 'len of y_train:', len(y_train))


  rf = RandomForestRegressor(n_estimators=100, max_depth=6, max_features=3)
  rf.fit(X_train, y_train)

  preds_train = rf.predict(X_train)
  preds_test = rf.predict(X_test)
  # print(preds_test)

  mlflow.log_params(rf.get_params())

  log_metrics(X_train, y_train, preds_train, label='train')
  log_metrics(X_test, y_test, preds_test, label='eval')

  signature = infer_signature(X_test, preds_test)
  mlflow.sklearn.log_model(rf, 'model', signature=signature)

  print('Run ID:', run.info.run_id)

# %%
