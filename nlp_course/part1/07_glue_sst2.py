#%% import packages
import evaluate
import numpy as np
from datasets import load_dataset
from torch.utils.data import DataLoader
from transformers import (AutoModelForSequenceClassification, AutoTokenizer,
                          DataCollatorWithPadding, Trainer, TrainingArguments)

#%% create dataloaders
raw_ds = load_dataset('glue', 'sst2')
checkpoint = 'bert-base-uncased'
tokenizer = AutoTokenizer.from_pretrained(checkpoint)

def tokenize_batch(batch):
  return tokenizer(batch['sentence'], truncation=True)

ds = raw_ds.map(tokenize_batch, batched=True)
ds = ds.remove_columns(['sentence', 'idx'])
ds = ds.rename_column('label', 'labels')
ds.set_format('torch')
data_collator = DataCollatorWithPadding(tokenizer=tokenizer)
bs = 256
train_dl = DataLoader(dataset=ds['train'],
                      shuffle=True,
                      batch_size=bs,
                      collate_fn=data_collator)
valid_dl = DataLoader(dataset=ds['validation'], batch_size=bs, collate_fn=data_collator)

#%% finetune model
model = AutoModelForSequenceClassification.from_pretrained(checkpoint, num_labels=2)
training_args = TrainingArguments(output_dir='bert-mrpc-sst2',
                                  num_train_epochs=4,
                                  evaluation_strategy='epoch',
                                  report_to='none'
                                  )
def compute_metrics(pred_labels):
  metric = evaluate.load('glue', 'sst2')
  logits, labels = pred_labels
  preds = np.argmax(logits, axis=-1)
  return metric.compute(predictions=preds, references=labels)

trainer = Trainer(model=model,
                  args=training_args,
                  train_dataset=ds['train'],
                  eval_dataset=ds['validation'],
                  data_collator=data_collator,
                  tokenizer=tokenizer,
                  compute_metrics=compute_metrics)

trainer.train()

# %%
