import requests
from bs4 import BeautifulSoup

def get_page_text(url):
    """Scrapes the text from a single page at the given URL."""
    response = requests.get(url)
    soup = BeautifulSoup(response.text, 'html.parser')
    article_element = soup.find('article', {'id': 'mcstories'})
    sections = article_element.find_all('section', recursive=False)
    text = []
    for section in sections:
        if section.find_parents('footer'):
            continue
        paragraphs = section.find_all('p')
        for paragraph in paragraphs:
            text.append(paragraph.text.replace('\n', ' '))
    return text

def scrape_pages(urls):
    """Scrapes the text from multiple pages at the given URLs."""
    text = []
    for url in urls:
        if isinstance(url, tuple):
            base_url, page_numbers = url
            for page_number in page_numbers:
                current_url = base_url.format(page_number)
                text.extend(get_page_text(current_url))
        else:
            text.extend(get_page_text(url))
    return text

def write_to_file(filename, data):
    """Writes the given data to a file with the given filename."""
    with open(filename, 'w') as f:
        f.write('text,\n')
        for line in data:
            f.write('"' + line + '",\n')

def main():
    urls = [
        ('https://mcstories.com/TamingTess/TamingTess{}.html', range(1, 16)),
        ('https://mcstories.com/BabyStepsTrancingEmily/BabyStepsTrancingEmily{}.html', range(1, 9)),
        ('https://mcstories.com/BabyStepsTrainingEmily/BabyStepsTrainingEmily{}.html', range(1, 9)),
        ('https://mcstories.com/BabyStepsAlternateEnding/BabyStepsAlternateEnding{}.html', range(1,3)),
        'https://mcstories.com/Bunker/Bunker.html',
        ('https://mcstories.com/Bunker/Bunker{}.html', range(2,4)),
        ('https://mcstories.com/Influencer/Influencer{}.html', range(1,21)),
        ('https://mcstories.com/RoleReversalMindSpark/RoleReversalMindSpark{}.html', range(1,8)),
        ('https://mcstories.com/SaintSylviesAcademy/SaintSylviesAcademy{}.html', range(1,9)),
        ('https://mcstories.com/GrimoireMindSpark/GrimoireMindSpark{}.html', range(1,16)),
        ('https://mcstories.com/MelinesManipulation/MelinesManipulation{}.html', range(1,5)),
        ('https://mcstories.com/NightTimeNudging/NightTimeNudging{}.html', range(1,16)),
        ('https://mcstories.com/SaintsAndSinners/SaintsAndSinners{}.html', range(1, 7)),
        ('https://mcstories.com/SporesSidia/SporesSidia{}.html', range(1,8)),
        ('https://mcstories.com/SporesTwo/SporesTwo{}.html', range(1, 13)),
        ('https://mcstories.com/StopWatch/StopWatch{}.html', range(1, 21)),
        ('https://mcstories.com/StopWatchGenesis/StopWatchGenesis{}.html', range(1, 6)),
        ('https://mcstories.com/Addictive/Addictive{}.html', range(1, 5)),
        ('https://mcstories.com/AngryAlister/AngryAlister{}.html', range(1, 4)),
        'https://mcstories.com/AnytimeAnywhere/AnytimeAnywhere.html',
        ('https://mcstories.com/Bartender/Bartender{}.html', range(1,6)),
        ('https://mcstories.com/BelowTheSurface/BelowTheSurface{}.html', range(1,7)),
        ('https://mcstories.com/BibleBelt/BibleBelt.html', range(1,4)),
        ('https://mcstories.com/BibleBeltRedux/BibleBeltRedux{}.html', range(1, 4)),
        ('https://mcstories.com/BigTitsTheory/BigTitsTheory{}.html', range(1,10)),
        ('https://mcstories.com/BlankBetty/BlankBetty.html', range(1,4)),
        'https://mcstories.com/BlowjobClass/BlowjobClass.html',
        'https://mcstories.com/CasualApproach/CasualApproach.html',
        ('https://mcstories.com/DatingMyDaughter/DatingMyDaughter{}.html', range(1, 7)),
        ('https://mcstories.com/DatingDennis/DatingDennis{}.html', range(1,3)),
        'https://mcstories.com/Daddy/Daddy.html',
        'https://mcstories.com/DominicsDiagnosis/DominicsDiagnosis.html',
        ('https://mcstories.com/ConvincingConnie/ConvincingConnie{}.html', range(1,6)),
        ('https://mcstories.com/DietPan/DietPan{}.html', range(1, 50)),
        'https://mcstories.com/Efficient/Efficient1.html',
        'https://mcstories.com/EveryEvening/EveryEvening.html',
        'https://mcstories.com/FamilyCircus/FamilyCircus.html',
        'https://mcstories.com/FiveDollarBets/FiveDollarBets1.html',
        'https://mcstories.com/FreeUseSister/FreeUseSister1.html',
        'https://mcstories.com/GarthsGirth/GarthsGirth.html',
        ('https://mcstories.com/HelpfulHannah/HelpfulHannah{}.html', range(1, 15)),
        ('https://mcstories.com/HierarchyOfNeeds/HierarchyOfNeeds{}.html', range(1, 27)),
        ('https://mcstories.com/HitchHiker/HitchHiker{}.html', range(1, 6)),
        'https://mcstories.com/LessonsPan/LessonsPan.html',
        ('https://mcstories.com/LevelTen/LevelTen{}.html', range(1, 4)),
        ('https://mcstories.com/Limits/Limits.html{}', range(2, 9)),
        ('https://mcstories.com/LovingLeah/LovingLeah{}.html', range(1, 6)),
        ('https://mcstories.com/Maids/Maids{}.html', range(1, 28)),
        ('https://mcstories.com/MassagedByMyNerdyBrother/MassagedByMyNerdyBrother{}.html', range(1,17)),
        ('https://mcstories.com/MoreLimits/MoreLimits{}.html', range(1, 9)),
        ('https://mcstories.com/MyBrotherWouldBeBetter/MyBrotherWouldBeBetter{}.html', range(1, 3)),
        ('https://mcstories.com/MyNudistSister/MyNudistSister{}.html', range(1, 5)),
        'https://mcstories.com/NoMoreLimits/NoMoreLimits1.html',
        'https://mcstories.com/PersuasivePablo/PersuasivePablo.html',
        ('https://mcstories.com/Pervert/Pervert{}.html', range(1, 5)),
        'https://mcstories.com/PunishingMyBrother/PunishingMyBrother.html',
        'https://mcstories.com/RandyRelations/RandyRelations.html',
        'https://mcstories.com/RandyRelations/RandyRelations2.html',
        'https://mcstories.com/SecondMachine/SecondMachine.html',
        'https://mcstories.com/SecondMachine/SecondMachine2.html',
        'https://mcstories.com/SelfControlPan/SelfControlPan.html',
        ('https://mcstories.com/SexyShorts/SexyShorts{}.html', range(1, 35)),
        ('https://mcstories.com/StrangerAtTheDoor/StrangerAtTheDoor{}.html', range(1, 3)),
        ('https://mcstories.com/TapesInTheAttic/TapesInTheAttic{}.html', range(1, 5)),
        ('https://mcstories.com/TeenWolfStepBrother/TeenWolfStepBrother{}.html', range(1, 4)),
        ('https://mcstories.com/ThatLook/ThatLook{}.html', range(1, 6)),
        'https://mcstories.com/TimeStop/TimeStop1.html',
        'https://mcstories.com/TwistedLovePotion/TwistedLovePotion3.html',
        ('https://mcstories.com/UnderThePanties/UnderThePanties{}.html', range(1, 7)),
        ('https://mcstories.com/Unlimited/Unlimited{}.html', range(1, 11)),
        ('https://mcstories.com/AprilIsTheCruelestMonth/AprilIsTheCruelestMonth{}.html', range(1, 5)),
        'https://mcstories.com/DoingStepdaughter/DoingStepdaughter.html',
        'https://mcstories.com/ForbiddenFruit/ForbiddenFruit.html'
    ]
    text = scrape_pages(urls)
    write_to_file('train.csv', text)

if __name__ == '__main__':
    main()

