#%% import packages
import datasets
from datasets import load_dataset
from torch.utils.data import DataLoader
from transformers import AutoTokenizer, AutoModelForSequenceClassification, DataCollatorWithPadding, TrainingArguments, Trainer

#%% prepare dataset
# !wget https://raw.githubusercontent.com/surge-ai/toxicity/main/toxicity_en.csv
comments = load_dataset('csv', data_files='toxicity_en.csv')
print('comments dataset:', comments)
comments = comments['train'].train_test_split(train_size=0.9, seed=42)
print('split dataset:', comments)

#%%
print('first few:', comments['train'][:3])

#%% prepare tokenized dataset

bert_checkpoint = 'bert-base-uncased'
tokenizer = AutoTokenizer.from_pretrained(bert_checkpoint)

_debug=True
def tokenize_batch(batch):
  global _debug
  if _debug:
    print('length of batch:', len(batch['text']))
    print('first few in the batch:', batch['text'][:3])
    _debug = False
  return tokenizer(batch['text'], truncation=True, max_length=512)

comments_tok = comments.map(tokenize_batch, batched=True)

#%% convert text column 'is_toxic' to numeric column 'labels' using map()
print('unique labels:', set(comments_tok['train']['is_toxic']))
def label_to_int(sample):
  if(sample['is_toxic'] == 'Toxic'):
    return { 'labels': 0 }
  else:
    return { 'labels': 1 }

comments_tok = comments_tok.map(label_to_int)
comments_tok = comments_tok.remove_columns(['text', 'is_toxic'])
print(comments_tok)
print('first few comments_tok:', comments_tok['train'][:3])

#%%
df = comments_tok['test'][:]
df['labels'] = df['is_toxic'].map({'Toxic': 0, 'Not_Toxic': 1})
comments_tok['test'] = df['labels']
comments_tok.reset_format()
comments_tok = comments_tok.remove_columns(['text', 'is_toxic'])
print(comments_tok)
print('first few comments_tok:', comments_tok['train'][:3])

#%% create data collator, model, training arguments and trainer objects and do the fine-tuning
comments_collator = DataCollatorWithPadding(tokenizer=tokenizer)

model = AutoModelForSequenceClassification.from_pretrained(bert_checkpoint, num_labels=2)

# train_dl = DataLoader(comments_tok['train'], batch_size=8, shuffle=True, collate_fn=comments_collator)
# test_dl = DataLoader(comments_tok['test'], batch_size=8, collate_fn=comments_collator)

training_args = TrainingArguments(output_dir='.',
                                  report_to='none',
                                  num_train_epochs=1,
                                  learning_rate=4e-5,
                                  evaluation_strategy='steps',
                                  save_strategy='epoch',)
print('device:', training_args.device)
trainer = Trainer(model=model,
                  args=training_args,
                  train_dataset=comments_tok['train'],
                  eval_dataset=comments_tok['test'],
                  data_collator=comments_collator,
                  tokenizer=tokenizer)
trainer.train()

#%%
# comment = 'Really good video, thanks for sharing the slides used in the presentation'
# comment = 'Despite a strong opening, the paper-thin narrative, stock characters, and repetitive combat mechanics do little to maintain interest'
comment = "You're a fucking idiot and your opinions don't matter. You're not even worth the time it takes to respond to your garbage."
encoded_comment = tokenizer(comment, return_tensors='pt')
print('encoded comment:', encoded_comment)

res = model(**encoded_comment)
print('toxic comment?', res.logits[0].argmax(-1))

#%% save and load the model
tokenizer_new = AutoTokenizer.from_pretrained('./checkpoint-113')
model_new = AutoModelForSequenceClassification.from_pretrained('./checkpoint-113', num_labels=2)
comment = "You're a fucking idiot and your opinions don't matter. You're not even worth the time it takes to respond to your garbage."
encoded_comment = tokenizer_new(comment, return_tensors='pt')
print('encoded comment:', encoded_comment)

res = model_new(**encoded_comment)
print('toxic comment?', res.logits[0].argmax(-1))

#%% evaluation metrics
## how to split into train/validation/test datasets
## calculate accuracy (and F1 score?)
## display train and validation loss over all the steps (per batch)
## tensorboard, wandb?
