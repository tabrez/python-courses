#%%
from transformers import AutoModelForCausalLM, AutoTokenizer

model_name = 'hakurei/lit-6B'
tokenizer = AutoTokenizer.from_pretrained(model_name)
model = AutoModelForCausalLM.from_pretrained(model_name, )

prompt = '''[ Title: The Dunwich Horror; Author: H. P. Lovecraft; Genre: Horror ]
***
When a traveler'''

input = tokenizer.encode(prompt, return_tensors='pt')
output = model.generate(input, do_sample=True, temperature=1.0, top_p=0.9, repetition_penalty=1.2, max_length=len(input[0]+100, pad_token_id=tokenizer.eos_token_id))

generated_text = tokenizer.decode(output[0])
print(generated_text)

