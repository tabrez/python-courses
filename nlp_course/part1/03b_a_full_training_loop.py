#%% import packages
import logging

import evaluate
import torch
from datasets import load_dataset
from torch.utils.data import DataLoader
from tqdm.auto import tqdm
from transformers import (AdamW, AutoModelForSequenceClassification,
                          AutoTokenizer, DataCollatorWithPadding,
                          get_scheduler)

logging.getLogger().setLevel(logging.ERROR)

#%% code we need from 03_finetuning_a_pretrained_model.py
raw_ds = load_dataset('glue', 'mrpc')
checkpoint = 'bert-base-uncased'
tokenizer = AutoTokenizer.from_pretrained(checkpoint)

def tokenize_function(sample):
  return tokenizer(sample['sentence1'], sample['sentence2'], truncation=True)

tokenized_ds = raw_ds.map(tokenize_function, batched=True)
data_collator = DataCollatorWithPadding(tokenizer=tokenizer)

#%% preparing for  training
tokenized_ds = tokenized_ds.remove_columns(['sentence1', 'sentence2', 'idx'])
tokenized_ds = tokenized_ds.rename_column('label', 'labels')
tokenized_ds.set_format('torch')
print('column names:', tokenized_ds['train'].column_names)

#%% create dataloaders
train_dl = DataLoader(tokenized_ds['train'], shuffle=True, batch_size=8, collate_fn=data_collator)
eval_dl = DataLoader(tokenized_ds['validation'], batch_size=8, collate_fn=data_collator)
for batch in train_dl:
  break
print('dataloader:', train_dl)
print('items:', {k: v.shape for k, v in batch.items()})

#%% create model, optimizer, & scheduler
model = AutoModelForSequenceClassification.from_pretrained(checkpoint, num_labels=2)
outputs = model(**batch)
print('loss:', outputs.loss, '\nlogits.shape:', outputs.logits.shape)

optimizer = AdamW(model.parameters(), lr=5e-5)

num_epochs = 3
num_training_steps = num_epochs * len(train_dl)
lr_scheduler = get_scheduler(
  'linear',
  optimizer=optimizer,
  num_warmup_steps=0,
  num_training_steps=num_training_steps
)

#%% create training loop
device = torch.device('cuda') if torch.cuda.is_available() else torch.device('cpu')
model.to(device)
print('device:', device)

progress_bar = tqdm(range(num_training_steps))

model.train()
for epoch in range(num_epochs):
  for batch in train_dl:
    batch = {k: v.to(device) for k, v in batch.items()}
    outputs = model(**batch)
    loss = outputs.loss
    loss.backward()

    optimizer.step()
    lr_scheduler.step()
    optimizer.zero_grad()
    progress_bar.update(1)

#%% create evaluation loop
metric = evaluate.load('glue', 'mrpc')
model.eval()
for batch in eval_dl:
  batch = {k: v.to(device) for k, v in batch.items()}
  with torch.no_grad():
    outputs = model(**batch)

  preds = torch.argmax(outputs.logits, dim=-1)
  metric.add_batch(predictions=preds, references=batch['labels'])
metric.compute()
