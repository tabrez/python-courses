# Huggingface llama2-7b-chat-hf minimal inference example

## Setup

```sh
python -m venv huggingface_env
source huggingface_env/bin/activate
pip install transformer torch ipykernel accelerate ipywidgets
huggingface-cli login
```
