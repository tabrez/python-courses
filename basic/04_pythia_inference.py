# https://huggingface.co/EleutherAI/pythia-70m
# https://huggingface.co/theblackcat102/pythia-3b-deduped-sft

# %%
from transformers import AutoTokenizer, GPTNeoXForCausalLM

model_name = 'EleutherAI/pythia-70m-deduped'
model = GPTNeoXForCausalLM.from_pretrained(
  model_name,
  # revision='step3000',
  # cache_dir="./pythia-70m-deduped/step3000",
).to('cuda')

tokenizer = AutoTokenizer.from_pretrained(
  model_name,
  # revision='step3000',
  # cache_dir="./pythia-70m-deduped/step3000",
)

inputs = tokenizer(
  'I was walking down the road when I saw a huge elephant',
  return_tensors='pt',
).to('cuda')
tokens = model.generate(
  **inputs,
  # early_stopping=True,
  max_new_tokens=80,
  do_sample=True,
  top_k=9,
  temperature=0.9,
  pad_token_id=tokenizer.eos_token_id,
)
tokenizer.decode(
  tokens[0],
  skip_special_tokens=True,
)

# %%
