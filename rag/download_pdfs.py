#%%
import os

import requests

url = "https://api.github.com/repos/dell-examples/generative-ai/contents/pytorch/RAG-llama2-custom-pdfs/pdfs-dell-infohub"
folder_location = r'pdfs_dell_infohub'
if not os.path.exists(folder_location):
  os.mkdir(folder_location)

response = requests.get(url)
files = response.json()
print('type of response:', type(response))
print('type of files:', type(files))

#%%
for file in files:
  print('name of the file:', file['name'], 'download url:', file['download_url'])
  if file['name'].endswith('.pdf'):
    pdf_url = file['download_url']
    pdf_response = requests.get(pdf_url)
    print('type of pdf_response:', type(pdf_response))
    filename = os.path.join(folder_location, file['name'])
    with open(filename, 'wb') as f:
      f.write(pdf_response.content)

# %%
