#%% import packages
import logging

import evaluate
import numpy as np
import torch
from datasets import load_dataset
from transformers import (AdamW, AutoModelForSequenceClassification,
                          AutoTokenizer, DataCollatorWithPadding, Trainer,
                          TrainingArguments)

logging.getLogger().setLevel(logging.ERROR)

#%%
checkpoint = 'bert-base-uncased'
tokenizer = AutoTokenizer.from_pretrained(checkpoint)
model = AutoModelForSequenceClassification.from_pretrained(checkpoint)
sequences = [
  "I've been waiting for a HuggingFace course my whole life.",
  "This course is amazing!",
]

batch = tokenizer(sequences, padding=True, truncation=True, return_tensors='pt')

# batch['labels'] = torch.tensor([1, 1])
batch['labels'] = torch.ones(1, len(sequences), dtype=torch.long).flatten()
print(f'batch: {batch["labels"]}')

optimizer = AdamW(model.parameters())
loss = model(**batch).loss
loss.backward()
optimizer.step()

#%% processing the data
raw_datasets = load_dataset('glue', 'mrpc')
print('dataset:', raw_datasets)
raw_train_dataset = raw_datasets['train']
print('\nfirst sample in train set:', raw_train_dataset[0])
print('\n15th sample in train set:', raw_train_dataset[15])
print('\n87th sample in validation set:', raw_datasets['validation'][87])
print('\ntrain features:', raw_train_dataset.features)

#%% how to pass data to tokenizer
## we can pass multiple lists of text to `tokenizer`
seq1 = 'The cat sat on the mat'
seq2 = 'She has a red balloon'
tokenized_seqs = tokenizer(seq1, seq2)
print('\ntokenized two seqs:', tokenized_seqs)
print(f'\ndecode: {tokenizer.convert_ids_to_tokens(tokenized_seqs["input_ids"])}')

list1 = ['The cat sat on the mat', 'I love to read books', 'He likes to play soccer']
list2 = ['She has a red balloon', 'We went to the park', 'They are eating dinner']
tokenized = tokenizer(list1, list2)
print('\ntokenized two lists, three columns:', tokenized)

#%% exercise:
## take element 15 of the training set and tokenize the two sentences
## separately and as a pair. What's the difference between the two results?
seq1 = raw_train_dataset[15]['sentence1']
seq1 = raw_train_dataset[15]['sentence1']
print('tokenize seq1:', tokenizer(seq1))
print('\ntokenize seq2:', tokenizer(seq2))
print('\ntokenize seq1, seq2:', tokenizer(seq1, seq2))

#%% tokenise the dataset
## we can retrieve all the values in `sentence1` by subscripting
sentence1_seq = raw_train_dataset['sentence1']
print('first few values of sentence1:', sentence1_seq[:3])
sentence2_seq =  raw_train_dataset['sentence2']
print('\nfirst few values of sentence2:', sentence2_seq[:3])

## returns a list of Encoding objects
print(f'\ntype: {type(raw_train_dataset["sentence1"])}')
tokenized = tokenizer(sentence1_seq, sentence2_seq, padding=True, truncation=True)
print('\ninputs:', tokenized[:4])

## returns a list
def tokenize_sample(sample):
  return tokenizer(sample['sentence1'], sample['sentence2'], truncation=True)

tokenized = raw_train_dataset.map(tokenize_sample, batched=True)
print('\ntokenized train ds:', tokenized[0].keys())

tokenized = raw_datasets.map(tokenize_sample, batched=True)
print('\ntokenized full ds', tokenized)

#%% dynamic padding with DataCollatorWithPadding
samples = tokenized['train'][:8]
samples = {k: v for k, v in samples.items() if k not in ['idx', 'sentence1', 'sentence2']}
print('len of first 8 training samples:', [len(x) for x in samples['input_ids']])

#%%
data_collator = DataCollatorWithPadding(tokenizer=tokenizer)
batch = data_collator(samples)
{k: v.shape for k, v in batch.items()}
# print('shape of values in collated batch:', {k: v.shape for k, v in batch.items()})

#%% exercise:
## replicate the preprocessing on the GLUE SST-2 dataset
# raw_glue = load_dataset('gimmaru/glue-sst2')
raw_glue = load_dataset('glue', 'sst2')
print('len of raw_glue:', len(raw_glue))
print('raw_glue:', raw_glue)

def tokenize_sample(sample):
  return tokenizer(sample['sentence'], truncation=True)

tokenized_glue = raw_glue.map(tokenize_sample, batched=True, remove_columns=['idx', 'sentence'])

## explore validation set
print('tokenized glue:', tokenized_glue)
tokenized_val_glue = tokenized_glue['validation']
print('tokenized val glue:', tokenized_val_glue)
print('len of tokenized val glue:', len(tokenized_val_glue))
print('first of tokenized val glue:', tokenized_val_glue[0])
# print('len of fields:', {k: len(v) for k, v in tokenized_val_glue[0].items()})

glue_collator = DataCollatorWithPadding(tokenizer=tokenizer)
glue_dict = {k: v for k, v in tokenized_val_glue[:].items()}
collated = glue_collator(glue_dict)
print('\ntype of collated:', type(collated))
print('len of collated:', len(collated))
print('\nfirst few of collated:', collated[:2])
print('\nshape of collated:', {k: v.shape for k, v in collated.items()})
print('\ncollated:', collated)

#%% training
## code from previous sections:
raw_datasets = load_dataset("glue", "mrpc")
checkpoint = "bert-base-uncased"
tokenizer = AutoTokenizer.from_pretrained(checkpoint)

def tokenize_function(example):
    return tokenizer(example["sentence1"], example["sentence2"], truncation=True)

tokenized_datasets = raw_datasets.map(tokenize_function, batched=True)
data_collator = DataCollatorWithPadding(tokenizer=tokenizer)

## new code
training_args = TrainingArguments('test-trainer')
model = AutoModelForSequenceClassification.from_pretrained(checkpoint, num_labels=2)

trainer = Trainer(
  model,
  training_args,
  train_dataset=tokenized_datasets['train'],
  eval_dataset=tokenized_datasets['validation'],
  data_collator=data_collator,
  tokenizer=tokenizer
)
print('device:', training_args.device)
trainer.train()


#%% evaluation
pred_output = trainer.predict(tokenized_datasets['validation'])

print('type of predictions:', type(pred_output))
print('pred_output fields:', pred_output._fields)
print('pred_output shapes:', pred_output.predictions.shape, pred_output.label_ids.shape)
preds = np.argmax(pred_output.predictions, axis=-1)
print('preds:', {preds.sum()})
print('label_ids:', {pred_output.label_ids.sum()})

metric = evaluate.load('glue', 'mrpc')
metric.compute(predictions=preds, references=pred_output.label_ids)

#%% compute metrics
def compute_metrics(pred_labels):
  # print(f'logits: {pred_labels[0][:3]}, labels: {pred_labels[1][:3]}')
  metric = evaluate.load('glue', 'mrpc')
  logits, labels = pred_labels
  preds = np.argmax(logits, axis=-1)
  return metric.compute(predictions=preds, references=labels)

#%% train with metrics
training_args = TrainingArguments('test-runner', evaluation_strategy='epoch')
model = AutoModelForSequenceClassification.from_pretrained(checkpoint, num_labels=2)

trainer = Trainer(
  model,
  training_args,
  train_dataset=tokenized_datasets['train'],
  eval_dataset=tokenized_datasets['validation'],
  data_collator=data_collator,
  tokenizer=tokenizer,
  compute_metrics=compute_metrics
)

trainer.train()

#%% exercise
## Fine-tune a model on the GLUE SST-2 dataset, using the data processing in previous section
checkpoint = 'bert-base-uncased'
tokenizer = AutoTokenizer.from_pretrained(checkpoint)
model = AutoModelForSequenceClassification.from_pretrained(checkpoint)

# https://huggingface.co/datasets/glue/viewer/sst2/train
raw_glue = load_dataset('glue', 'sst2')
def tokenize_sample(sample):
  return tokenizer(sample['sentence'], truncation=True)
tokenized_glue = raw_glue.map(tokenize_sample, batched=True, remove_columns=['idx', 'sentence'])
glue_collator = DataCollatorWithPadding(tokenizer=tokenizer)

def compute_metrics(pred_labels):
  metric = evaluate.load('glue', 'sst2')
  logits, labels = pred_labels
  preds = np.argmax(logits, axis=-1)
  return metric.compute(predictions=preds, references=labels)

glue_training_args = TrainingArguments('test-trainer',
                                       evaluation_strategy='epoch',
                                       report_to='none')
trainer = Trainer(
  model,
  training_args,
  train_dataset=tokenized_glue['train'],
  eval_dataset=tokenized_glue['validation'],
  data_collator=glue_collator,
  tokenizer=tokenizer,
  compute_metrics=compute_metrics,
)

trainer.train()

#%% a full training
## 03b_a_full_training.py
