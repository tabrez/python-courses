#%% imports
import torch
from torch import nn
from torch.utils.data import DataLoader
from torchvision.transforms import ToTensor
import torchvision.datasets as td

#%% create datasets
train_ds = td.FashionMNIST(root='data',
                          train=True,
                          transform=ToTensor(),
                          download=True)

valid_ds = td.FashionMNIST(root='data',
                          train=False,
                          transform=ToTensor(),
                          download=True)

#%% create dataloaders
batch_size=64
train_dl = DataLoader(train_ds, batch_size=batch_size)
valid_dl = DataLoader(valid_ds, batch_size=batch_size)

for X, y in valid_dl:
  print(f'X: {X[0][0][0]}, y: {y[0]}')
  print(f'Shape of X [N, C, H, W]: {X.shape}')
  print(f'Shape of y: {y.shape}, type of y: {y.dtype}')
  break

#%% create model
device = ("cuda" if torch.cuda.is_available() else "cpu")
print(f"Using device {device}")

class NeuralNetwork(nn.Module):
  def __init__(self):
    super().__init__()
    self.flatten = nn.Flatten()
    self.linear_relu_stack = nn.Sequential(
      nn.Linear(28*28, 512),
      nn.ReLU(),
      nn.Linear(512, 512),
      nn.ReLU(),
      nn.Linear(512, 10)
    )

  def forward(self, x):
    x = self.flatten(x)
    logits = self.linear_relu_stack(x)
    return logits

model = NeuralNetwork()
print(model)

#%% loss function and optimizer
loss_fn = nn.CrossEntropyLoss()
optimizer = torch.optim.SGD(model.parameters(), lr=1e-3)

#%% train
def train(dl, model, loss_fn, optimizer):
  for batch, (X, y) in enumerate(dl):
    X, y = X.to(device), y.to(device)

    preds = model(X)
    loss = loss_fn(preds, y)
    loss.backward()
    optimizer.step()
    optimizer.zero_grad()

    if batch % 100 == 0:
      current = (batch + 1) * len(X)
      print(f'loss: {loss.item()}, {current}/{len(dl.dataset)}')

#%% training loop
epochs = 5
for i in range(epochs):
  print(f'Epoch {i}=======================')
  train(train_dl, model, loss_fn, optimizer)
print('Training done.')

#%% save trained model
model.save(model.state_dict(), "model.pth")
print('saved the trained model')

#%% inference
# mode = NeuralNetwork().to(device)
# model = model.load_state_dict(torch.load('model.pth'))

# model.eval()
count = 0
for i in range(len(valid_ds)):
  X, y = valid_ds[i][0], valid_ds[i][1]
  pred_raw = model(X)
  pred = pred_raw[0].argmax()
  if(pred != y):
    print(f'i: {i}, pred: {pred}, y: {y}')
    count += 1
print(f'incorrect classifcations: {count}/{len(valid_ds)}')
