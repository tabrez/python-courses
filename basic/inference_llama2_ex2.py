#%% inference example using llama2 model
# https://huggingface.co/docs/transformers/llm_tutorial

from transformers import AutoTokenizer, AutoModelForCausalLM

model_name = 'openlm-research/open_llama_7b'
tokenizer = AutoTokenizer.from_pretrained(model_name)
tokenizer.pad_token = tokenizer.eos_token
model = AutoModelForCausalLM.from_pretrained(model_name, device_map='auto', load_in_4bit=True)

model_inputs = tokenizer(['You\'re a wizard, Harry.'], return_tensors='pt').to('cuda')
model_outputs = model.generate(**model_inputs, max_new_tokens=200)
output = tokenizer.batch_decode(model_outputs, skip_special_tokens=True)[0]
print(f'output: {output}')

#%%
# The tokenizer initialized above has right-padding active by default: the 1st sequence,
# which is shorter, has padding on the right side. Generation fails.
model_inputs = tokenizer(
    ["1, 2, 3", "A, B, C, D, E"], padding=True, return_tensors="pt"
).to("cuda")
generated_ids = model.generate(**model_inputs)
output = tokenizer.batch_decode(generated_ids[0], skip_special_tokens=True)[0]
print(f'output for inputs with different size: {output}')

#%%
# With left-padding, it works as expected!
tokenizer = AutoTokenizer.from_pretrained(model_name, padding_side='left')
tokenizer.pad_token = tokenizer.eos_token
model_inputs = tokenizer(
    ["1, 2, 3", "A, B, C, D, E"], padding=True, return_tensors="pt"
).to("cuda")
generated_ids = model.generate(**model_inputs, max_new_tokens=200)
output = tokenizer.batch_decode(generated_ids, skip_special_tokens=True)[0]
print(f'output for inputs with different size and left padding: {output}')
