# %% import packages
import evaluate
import nltk
import numpy as np
from datasets import load_dataset
from transformers import (
  AutoModelForSeq2SeqLM,
  AutoModelForSequenceClassification,
  AutoTokenizer,
  DataCollatorForSeq2Seq,
  Seq2SeqTrainer,
  Seq2SeqTrainingArguments,
  Trainer,
  TrainingArguments,
)

# %% evaluate transformer model using accuracy metric
dataset = load_dataset('yelp_review_full')
tokenizer = AutoTokenizer.from_pretrained('bert-base-cased')


def tokenize_function(examples):
  return tokenizer(examples['text'], padding='max_length', truncation=True)


tokenized_datasets = dataset.map(tokenize_function, batched=True)
small_train_ds = tokenized_datasets['train'].shuffle(seed=42).select(range(200))
small_test_ds = tokenized_datasets['test'].shuffle(seed=42).select(range(200))

# setup evaluation
metric = evaluate.load('accuracy')


def compute_metrics(eval_pred):
  logits, labels = eval_pred
  predictions = np.argmax(logits, axis=-1)
  return metric.compute(predictions=predictions, references=labels)


# Load pre-trained model and evaluate mode after each epoch
model = AutoModelForSequenceClassification.from_pretrained(
  'bert-base-cased', num_labels=5
)
training_args = TrainingArguments(
  output_dir='./evaluate_transformer', evaluation_strategy='epoch'
)

trainer = Trainer(
  model=model,
  args=training_args,
  train_dataset=small_train_ds,
  eval_dataset=small_test_ds,
  compute_metrics=compute_metrics,
)

trainer.train()

# %% evaluate seq2seq models using rouge metric
# tokenize billsum dataset
billsum = load_dataset('billsum', split='ca_test').shuffle(seed=42).select(range(200))
print('billsum before split:', billsum)
billsum = billsum.train_test_split(test_size=0.2)
print('billsum before split:', billsum)

tokenizer = AutoTokenizer.from_pretrained('t5-small')
prefix = 'summarise: '


def preprocess_function(examples):
  inputs = [prefix + doc for doc in examples['text']]
  model_inputs = tokenizer(inputs, max_length=1024, truncation=True)
  # print('model_inputs:', model_inputs)
  print('model_inputs keys:', model_inputs.keys())
  print('first input_ids of model_inputs:', model_inputs['input_ids'][0][:3])
  labels = tokenizer(text_target=examples['summary'], max_length=128, truncation=True)
  model_inputs['labels'] = labels['input_ids']
  print('model_inputs keys after adding labels:', model_inputs.keys())
  print('first labels of model_inputs:', model_inputs['labels'][0][:3])
  return model_inputs


tokenized_billsum = billsum.map(preprocess_function, batched=True)
print('tokenized_billsum:', tokenized_billsum)
print('columns of tokenized_billsum:', tokenized_billsum['train'].column_names)

# setup metrics
nltk.download('punkt', quiet=True)
metric = evaluate.load('rouge')


def compute_metrics(eval_preds):
  preds, labels = eval_preds

  # decode preds and labels
  labels = np.where(labels != -100, labels, tokenizer.pad_token_id)
  decoded_preds = tokenizer.batch_decode(preds, skip_special_tokens=True)
  decoded_labels = tokenizer.batch_decode(labels, skip_special_tokens=True)

  # rougeLSum expects newline after each sentence
  decoded_preds = [
    '\n'.join(nltk.sent_tokenize(pred.strip())) for pred in decoded_preds
  ]
  decoded_labels = [
    '\n'.join(nltk.sent_tokenize(labels.strip())) for labels in decoded_labels
  ]

  return metric.compute(
    predictions=decoded_preds, references=decoded_labels, use_stemmer=True
  )


# load pre-trained model and evaluate model after each epoch
model = AutoModelForSeq2SeqLM.from_pretrained('t5-small')
data_collator = DataCollatorForSeq2Seq(tokenizer=tokenizer, model=model)

training_args = Seq2SeqTrainingArguments(
  output_dir='./results',
  evaluation_strategy='epoch',
  learning_rate=2e-5,
  per_device_train_batch_size=16,
  per_device_eval_batch_size=4,
  weight_decay=0.01,
  save_total_limit=3,
  num_train_epochs=2,
  # fp16=True,
  predict_with_generate=True,
)

trainer = Seq2SeqTrainer(
  model=model,
  args=training_args,
  train_dataset=tokenized_billsum['train'],
  eval_dataset=tokenized_billsum['test'],
  tokenizer=tokenizer,
  data_collator=data_collator,
  compute_metrics=compute_metrics,
)

trainer.train()
