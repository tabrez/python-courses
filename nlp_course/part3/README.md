# Fine-tune a large language model using Huggingface library

## Goal

Create a fine-tuned model for text classification purposes using a flavour of `bert-base` model and any
version of comment toxicity dataset.

## Preparation

[BERT](https://huggingface.co/bert-base-uncased): `bert-base-uncased` 110M parameter model
[Toxicity dataset](https://github.com/surge-ai/toxicity): `toxicity_en.csv` dataset with 500 toxic 
and non-toxic comments each

`wget https://raw.githubusercontent.com/surge-ai/toxicity/main/toxicity_en.csv`
