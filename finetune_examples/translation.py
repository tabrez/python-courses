# https://huggingface.co/docs/transformers/tasks/translation
# %% import packages
import time
from typing import List, Tuple, cast

import evaluate
import numpy as np
from datasets import Dataset, DatasetDict, load_dataset
from transformers import (
  AutoModelForSeq2SeqLM,
  AutoTokenizer,
  DataCollatorForSeq2Seq,
  Seq2SeqTrainer,
  Seq2SeqTrainingArguments,
)


# %% helper utils
def time_function(func):
  def wrapper(*args, **kwargs):
    start = time.time()
    result = func(*args, **kwargs)
    elapsed_time = time.time() - start

    if elapsed_time < 60:
      print(f'Executed time: {elapsed_time:.2f} secs')
    else:
      print(f'Executed time: {elapsed_time / 60:.2f} mins')
    return result

  return wrapper


# %% load the dataset
books: DatasetDict = cast(DatasetDict, load_dataset('opus_books', 'en-fr'))
books_train = cast(Dataset, books['train'])
books = books['train'].train_test_split(test_size=0.2)
print('books:', books)
print('first of books:', books['train'][0])

# %% tokenize the dataset
checkpoint = 't5-small'
tokenizer = AutoTokenizer.from_pretrained(checkpoint)
source_lang = 'en'
target_lang = 'fr'
prefix = 'translate English to French: '

# TODO: find out the max length of source and target language texts in `books`

_debug = True


def preprocess_function(examples):
  global _debug
  inputs = [prefix + example[source_lang] for example in examples['translation']]
  targets = [example[target_lang] for example in examples['translation']]
  model_inputs = tokenizer(
    inputs, text_target=targets, max_length=128, truncation=True, padding=True
  )
  if _debug:
    print('model_inputs:', model_inputs['labels'][0])
    print('length of model_inputs:', len(model_inputs['labels'][0]))
  _debug = False
  return model_inputs


tokenized_books: DatasetDict = books.map(preprocess_function, batched=True)
print(tokenized_books)
print('first of ds:', tokenized_books['train'][0])

# data_collator = DataCollatorForSeq2Seq(tokenizer=tokenizer, model=checkpoint)

# %% evaluation of the model performance
_debug = False
ntr = 3 if _debug else len(tokenized_books['train'])
nte = 3 if _debug else len(tokenized_books['test'])

ds_train: Dataset = tokenized_books['train'].shuffle(seed=42).select(range(ntr))
ds_eval: Dataset = tokenized_books['test'].select(range(nte))


def compute_metrics(eval_preds):
  def postprocess_text(
    preds: List[str], labels: List[str]
  ) -> Tuple[List[str], List[List[str]]]:
    new_preds: List[str] = [pred.strip() for pred in preds]
    new_labels: List[List[str]] = [[label.strip()] for label in labels]

    return new_preds, new_labels

  preds, labels = eval_preds
  print('\npredictions', preds)
  print('\nlabels:', labels)
  for i in range(len(eval_preds[0])):
    print('\nlen of predictions', len(preds[i]))
    print('\nlen of labels:', len(labels[i]))
  # print('\nfirst label in eval dataset:', ds_eval[0]['labels'])
  if isinstance(preds, tuple):
    preds = preds[0]
    print('preds:', preds)

  _decoded_preds: List[str] = tokenizer.batch_decode(preds, skip_special_tokens=True)

  labels = np.where(labels != -100, labels, tokenizer.pad_token_id)
  _decoded_labels: List[str] = tokenizer.batch_decode(labels, skip_special_tokens=True)

  # print('\ndecoded predictions before postprocess:', decoded_preds)
  # print('\ndecoded labels before postprocess:', decoded_labels)
  decoded_preds, decoded_labels = postprocess_text(_decoded_preds, _decoded_labels)
  # print('\ndecoded predictions:', decoded_preds)
  # print('\ndecoded labels:', decoded_labels)

  # print('\n decode 673:', tokenizer.batch_decode([[673]]))
  # print('\n decode 1022:', tokenizer.batch_decode([[1022]]))
  # print('\n decode 10010:', tokenizer.batch_decode([[10010]]))
  # print('\n decode 197:', tokenizer.batch_decode([[197]]))
  # print('\n decode 3:', tokenizer.batch_decode([[3]]))
  # print('\n decode 17380:', tokenizer.batch_decode([[17380]]))
  # print('\n decode 0:', tokenizer.batch_decode([[0]]))

  metric = evaluate.load('sacrebleu')
  result: dict | None = metric.compute(
    predictions=decoded_preds, references=decoded_labels
  )
  if result is None:
    return {}
  prediction_lens = [np.count_nonzero(pred != tokenizer.pad_token_id) for pred in preds]
  result_bleu_np: dict[str, np.floating] = {'gen_len': np.mean(prediction_lens)}
  # TODO: print average length of text in labels column of evaluation dataset
  result_dict = {k: round(v, 4) for k, v in result_bleu_np.items()}
  result_dict['bleu'] = result['score']

  return result_dict


# %% fine-tune the model
output_dir = '/home/tabrez/.huggingface/results/t5-small-opus-books-tutorial'
model = AutoModelForSeq2SeqLM.from_pretrained(checkpoint)
tokenizer.model_max_length = 128
training_args = Seq2SeqTrainingArguments(
  output_dir=output_dir,
  evaluation_strategy='epoch',
  learning_rate=2e-5,
  per_device_train_batch_size=16,
  per_device_eval_batch_size=16,
  weight_decay=0.01,
  save_total_limit=3,
  num_train_epochs=2,
  predict_with_generate=True,
  generation_max_length=128,
  fp16=True,
  push_to_hub=True,
)

trainer = Seq2SeqTrainer(
  model=model,
  args=training_args,
  train_dataset=ds_train,
  eval_dataset=ds_eval,
  tokenizer=tokenizer,
  # data_collator=data_collator,
  compute_metrics=compute_metrics,
)

before_preds = trainer.predict(ds_eval)
# TODO: how to estimate how much time training might take
# time_function(trainer.train)()
trainer.train()

# %% inspect before_preds
# print('predictions:', before_preds.predictions)
for i, preds in enumerate(before_preds.predictions):
  print('predictions:', preds, 'len(preds):', len(preds))
  if i == 5:
    break
for i, labels in enumerate(ds_eval['labels']):
  print('labels:', labels, 'len(labels):', len(labels))
  if i == 5:
    break
# print('labels:', np.array(ds_eval['labels']))
# results = compute_metrics((before_preds, ds_eval['labels']))
# print('results:', results)

# %% prediction on one sample using model object
text = 'I woke up in the morning with a splitting headache and went straight to my coffee machine.'
input = tokenizer(prefix + text, return_tensors='pt').to('cuda:0')
print('input:', input)
outputs = model.generate(input.input_ids, max_new_tokens=40)
print('output:', tokenizer.decode(outputs[0]))

# %% check if model and input are on cpu or gpu device
# when model is created like below, it will be on cpu by default
model = AutoModelForSeq2SeqLM.from_pretrained(checkpoint)
device = next(model.parameters()).device
print('model device:', model.device)
# trainer object moves the model and data passed to it to the GPU if it's available
# so we need to put our data on gpu also
input = tokenizer(prefix + 'Stock markets have crashed today', return_tensors='pt').to(
  'cuda:0'
)
print('input device:', input.input_ids.device)
# or move model back to cpu
model.to('cpu')

# %%
