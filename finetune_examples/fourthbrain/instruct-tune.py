#%% import packages
import os
os.environ['CUDA_LAUNCH_BLOCKING'] = '1'

from datasets import load_dataset
import torch
import transformers
from transformers import LlamaTokenizer
from peft import LoraConfig, get_peft_model
from transformers import AutoModelForCausalLM, BitsAndBytesConfig, AutoTokenizer
import matplotlib.pyplot as plt
from trl import SFTTrainer
from IPython.display import display, Markdown

#%% load dataset
dbricks_15k_dataset_base = load_dataset('databricks/databricks-dolly-15k')
print('dataset:', dbricks_15k_dataset_base)

# %%
def plot_sequence_lengths(dataset_obj):
    sequence_lengths = []
    too_long = []

    for idx, example in enumerate(dataset_obj["train"]):
        sequence_lengths.append(len(example['instruction']) + len(example["context"]) + len(example["response"]))
        if sequence_lengths[idx] > 2200:
          too_long.append(idx)

    plt.hist(sequence_lengths, bins=30)
    plt.xlabel('Sequence Length')
    plt.ylabel('Count')
    plt.title('Distribution of Text Sequence Lengths')
    plt.show()

    return too_long

indexes_to_drop = plot_sequence_lengths(dbricks_15k_dataset_base)

# %%
print('lenght of indexes to drop:', len(indexes_to_drop))
dbricks_15k_dataset_reduced = dbricks_15k_dataset_base['train'].select(
  i for i in range(len(dbricks_15k_dataset_base['train'])) if i not in set(indexes_to_drop)
)
print('reduced dataset:', dbricks_15k_dataset_reduced)

#%%
dbricks_15k_dataset_prepared = dbricks_15k_dataset_reduced.train_test_split(test_size=0.1)
indexes_to_drop = plot_sequence_lengths(dbricks_15k_dataset_prepared)
print('indexes to drop', indexes_to_drop)
print('prepared dataset:', dbricks_15k_dataset_prepared)

#%% format dataset columns
def formatting_func(example):
  if example.get("context", "") != "":
      input_prompt = (f"Below is an instruction that describes a task, paired with an input that provides further context. "
      "Write a response that appropriately completes the request.\n\n"
      "### Instruction:\n"
      f"{example['instruction']}\n\n"
      f"### Input: \n"
      f"{example['context']}\n\n"
      f"### Response: \n"
      f"{example['response']}")

  else:
    input_prompt = (f"Below is an instruction that describes a task. "
      "Write a response that appropriately completes the request.\n\n"
      "### Instruction:\n"
      f"{example['instruction']}\n\n"
      f"### Response:\n"
      f"{example['response']}")

  return {"text" : input_prompt}

formatted_dataset = dbricks_15k_dataset_prepared.map(formatting_func)
print('formatted dataset:', formatted_dataset)
print('3rd sample:', formatted_dataset['train'][2]['text'])

#%%
torch.cuda.empty_cache()

#%%
# model_id = "openlm-research/open_llama_7b_700bt_preview"
model_id = "openlm-research/open_llama_7b_v2"

qlora_config = LoraConfig(
    r=16,
    lora_alpha=32,
    lora_dropout=0.05,
    bias="none",
    task_type="CAUSAL_LM"
)

bnb_config = BitsAndBytesConfig(
    load_in_4bit=True,
    bnb_4bit_use_double_quant=True,
    bnb_4bit_quant_type="nf4",
    bnb_4bit_compute_dtype=torch.bfloat16
)

base_model = AutoModelForCausalLM.from_pretrained(
    model_id,
    quantization_config=bnb_config,
)

# %%
tokenizer = LlamaTokenizer.from_pretrained(model_id)
tokenizer.add_special_tokens({'pad_token': '[PAD]'})

print(base_model)

#%%
supervised_finetuning_trainer = SFTTrainer(
    base_model,
    train_dataset=formatted_dataset["train"],
    eval_dataset=formatted_dataset["test"],
    args=transformers.TrainingArguments(
        per_device_train_batch_size=1,
        gradient_accumulation_steps=4,
        learning_rate=2e-4,
        max_steps=5000,
        output_dir="./SFTOpenLM-Dolly15k",
        optim="paged_adamw_8bit",
        fp16=True,
    ),
    tokenizer=tokenizer,
    peft_config=qlora_config,
    dataset_text_field="text",
    max_seq_length=512
)

supervised_finetuning_trainer.train()

#%% bing chat ai save suggestion
supervised_finetuning_trainer.model.save_pretrained("./Bing-SFTOpenLM-Dolly15k")
supervised_finetuning_trainer.model.config.save_pretrained("./Bing-SFTOpenLM-Dolly15k")
supervised_finetuning_trainer.tokenizer.save_pretrained("./Bing-SFTOpenLM-Dolly15k")

#%% try GPTQQuantizer

#%%
supervised_finetuning_trainer.save_model('llama2-dolly15k-model')
supervised_finetuning_trainer.save_config('llama2-dolly15k-model/config.json')

#%% save finetuned model locally
tokenizer.save_pretrained('llama2-dolly15k-tok')
base_model.save_pretrained('llama2-dolly15k-model')
# supervised_finetuning_trainer.save_model('llama2-dolly15k-model')

#%%
base_model.push_to_hub("itabrez/FB-DLAI-Instruct-tune-v3")
tokenizer.push_to_hub("itabrez/FB-DLAI-Instruct-tune-v3")

#%%
# lora_config = LoraConfig.from_pretrained("FourthBrainGenAI/FB-DLAI-Instruct-tune-v3")
# bnb_config = BitsAndBytesConfig(
#     load_in_4bit=True,
#     bnb_4bit_use_double_quant=True,
#     bnb_4bit_quant_type="nf4",
#     bnb_4bit_compute_dtype=torch.bfloat16
# )

# tokenizer = AutoTokenizer.from_pretrained("FourthBrainGenAI/FB-DLAI-Instruct-tune-v3")
# model = AutoModelForCausalLM.from_pretrained(
#     lora_config.base_model_name_or_path,
#     quantization_config=bnb_config,
#     device_map={"":0})
# model = get_peft_model(model, lora_config)

#%%
print('base model:', base_model)

#%%
# model = supervised_finetuning_trainer.load_model('llama2-dolly15k-model')
model = AutoModelForCausalLM.from_pretrained(
    'llama2-dolly15k-model',
        # quantization_config=bnb_config
)

#%%
# Load the model
model = AutoModelForCausalLM.from_pretrained("./Bing-SFTOpenLM-Dolly15k")

# Load the tokenizer
tokenizer = LlamaTokenizer.from_pretrained("./Bing-SFTOpenLM-Dolly15k")

def make_inference(instruction, context = None):
  if context:
    prompt = f"Below is an instruction that describes a task, paired with an input that provides further context.\n\n### Instruction: \n{instruction}\n\n### Input: \n{context}\n\n### Response: \n"
  else:
    prompt = f"Below is an instruction that describes a task. Write a response that appropriately completes the request.\n\n### Instruction: \n{instruction}\n\n### Response: \n"
  inputs = tokenizer(prompt, return_tensors="pt", return_token_type_ids=False).to("cuda:0")
  outputs = model.generate(**inputs, max_new_tokens=100)
  display(Markdown((tokenizer.decode(outputs[0], skip_special_tokens=True))))
  outputs = base_model.generate(**inputs, max_new_tokens=50)
  print("---- NON-INSTRUCT-TUNED-MODEL ----")
  display(Markdown((tokenizer.decode(outputs[0], skip_special_tokens=True))))

make_inference("Convert the text into a dialogue between two characters.", "Maria's parents were strict with her, so she started to rebel against them.")
# make_inference("Explain in simple terms how the attention mechanism of a transformer model works")
# make_inference("Identify the odd one out and explain your choice.", "Orange, Green, Airplane.")

# %%
