## https://www.loc.gov/item/2020445568/

#%% import packages
from langchain.document_loaders import PyPDFDirectoryLoader #, PyPDFLoader
from langchain.text_splitter import RecursiveCharacterTextSplitter
import chromadb, time

#%%
# loader = PyPDFLoader('data/gov_dataset/pdfs/data/23JCEIKVL65X2RGSARNY2VOUNXBTJ5AS.pdf')
# docs = loader.load_and_split()

## TODO: Load each file manually and add the name of the file and chunk number in the metadatas
loader = PyPDFDirectoryLoader('data/gov_dataset/pdfs/filtered')
docs = loader.load()
print('type of docs:', type(docs), 'len of docs:', len(docs), 'type of docs[0]:', type(docs[0]))

splitter = RecursiveCharacterTextSplitter(
  chunk_size=400,
  chunk_overlap=40,
  length_function=len,
  add_start_index=True,
)

documents = splitter.split_documents(docs)
print('type of documents:', type(documents), 'type of documents[0]', type(documents[0]))

ids = []
for i, doc in enumerate(documents):
  ids.append(str(i))

#%%
client = chromadb.PersistentClient('./db')
collection_name = 'library_of_congress'
# if collection_name in client.list_collections():
client.delete_collection(collection_name)
collection = client.create_collection(collection_name)
start = time.time()
collection.add(documents=list(map(lambda d: d.page_content, documents)), ids=ids)
print('embeddings added to collection in ', time.time() - start, ' ms')

#%%
query1 = 'Do Not Call list'
query2 = 'Foreign tax credit allowed to shareholders.'
results = collection.query(query_texts=[query1, query2], n_results=3)
print('type of results:', type(results), 'keys:', results.keys())
print('type of results.documents:', type(results['documents']), 'len of results.documents:', len(results['documents']))

for res in results  ['documents']:
  for r in res:
    print('\n==================================================')
    print('\nres:', r)

#%%
