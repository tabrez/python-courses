# %% import packages
import time

from transformers import (
  AutoModelForSeq2SeqLM,
  AutoTokenizer,
)


# %% helper utils
def time_function(func):
  def wrapper(*args, **kwargs):
    start = time.time()
    result = func(*args, **kwargs)
    elapsed_time = time.time() - start

    if elapsed_time < 60:
      print(f'Executed time: {elapsed_time:.2f} secs')
    else:
      print(f'Executed time: {elapsed_time / 60:.2f} mins')
    return result

  return wrapper


# %% create model & tokenizer
model_path = 't5-small'
tokenizer = AutoTokenizer.from_pretrained(model_path, model_max_length=512)
model = AutoModelForSeq2SeqLM.from_pretrained(model_path, max_length=512)
# model = T5ForConditionalGeneration.from_pretrained(model_path, max_length=512)
print('max length:', model.config.max_length)


@time_function
def predict(input_text):
  input_ids = tokenizer.encode(
    input_text, max_length=300, truncation=True, return_tensors='pt'
  )
  outputs = model.generate(input_ids)
  return tokenizer.decode(outputs[0], skip_special_tokens=True)


# %% translate from Englis to German
input_text = 'Translate English to German: Studies have been shown that owning a dog is good for you'
tokenized_text = tokenizer.tokenize(input_text)
print('tokenized text:', tokenized_text)
input_ids = tokenizer.encode(
  input_text, max_length=300, truncation=True, return_tensors='pt'
)
print('input_ids:', input_ids)
outputs = model.generate(input_ids)
print('outputs', outputs)
print('len of outputs', len(outputs))
print('len of outputs[0]', len(outputs[0]))
decoded_output = tokenizer.decode(outputs[0])
print(decoded_output)

# %% summarise text
input_text = """summarize: Ah, the text style you're referring to is often known as
"Mocking SpongeBob" or "SpongeBob chicken meme" text. It's used to convey a mocking or
sarcastic tone, mimicking the way someone might teasingly repeat what another person
has said. The text alternates between uppercase and lowercase letters in a seemingly
random pattern. Here's an example:

Original statement: "I don't think we need to back up our files."
Mocking SpongeBob format: "I DoN't ThInK wE nEeD tO bAcK uP oUr FiLeS."

As for generating this kind of text with a large language model, most models like GPT-3
or its variants aren't designed to inherently generate text in this mocking format.
They are usually trained to produce grammatically correct and contextually appropriate
responses. However, you could programmatically transform the output of a language model
into this format by alternating the casing of each letter in a post-processing step.
This approach would involve writing a simple script to process the text, rather than
relying on the language model itself to generate text in that style."""

decoded_output = predict(input_text)
print(decoded_output)

# %% question-answer task
context1 = """Primary colours are a set of colours that cannot be created by mixing other colours together. The three most common primary colours are red, yellow, and blue."""

context2 = """
The first known use of red, yellow, and blue as "simple" or "primary" colors, by Chalcidius, ca. AD 300, was possibly based on the art of paint mixing.

Mixing pigments for the purpose of creating realistic paintings with diverse color gamuts is known to have been practiced at least since Ancient Greece (see history section). The identity of a/the set of minimal pigments to mix diverse gamuts has long been the subject of speculation by theorists whose claims have changed over time, for example, Pliny's white, black, one or another red, and "sil", which might have been yellow or blue; Robert Boyle's white, black, red, yellow, and blue; and variations with more or fewer "primary" color or pigments.
"""

input_text = f'question: What are the three primary colours? context: {context2}'
decoded_output = predict(input_text)
print(decoded_output)

# %% Text classification/sentiment analysis
input_text = """sentiment: Polls showed that most voters supported limiting pollution and wanted companies to be more responsible stewards of the land. John hoped politicians would listen to what the people wanted and pass laws reflecting these concerns. sentiment: """
input_text = """sentiment: I love sunny days."""
decoded_output = predict(input_text)
print(decoded_output)

# %% t5-small support translation from English to German, French & Romanian
# translation from English to Spanish is not supported
input_text = 'Translate English to Spanish: Studies have been shown that owning a dog is good for you'
tokenized_text = tokenizer.tokenize(input_text)
input_ids = tokenizer.encode(
  input_text, max_length=300, truncation=True, return_tensors='pt'
)
outputs = model.generate(input_ids)
decoded_output = tokenizer.decode(outputs[0])
print(decoded_output)  # incorrect output

# %% fine-tune t5-small on english to spanish translation task
# see finetune_examples/t5-base-french-translation.py
# see finetune_examples/t5-base-spanish-translation.py
