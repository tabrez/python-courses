#%% run the following commands in the shell
# pip install -q autotrain-advanced huggingface_hub
# huggingface-cli login --token $HF_TOKEN
time autotrain llm \
  --train \
  --project_name autotrain_example \
  --model meta-llama/Llama-2-7b-hf \
  --data_path . \
  --use_peft \
  --use_int4 \
  --learning_rate 2e-4 \
  --train_batch_size 24 \
  --num_train_epochs 3 \
  --trainer sft \
  --model_max_length 2048 \
  --push_to_hub \
  --repo_id itabrez/llama2-mindspark
