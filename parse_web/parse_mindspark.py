import requests
from bs4 import BeautifulSoup

# Set the base URLs of the pages to scrape and their corresponding page number ranges
base_urls = [
    ('https://mcstories.com/TamingTess/TamingTess{}.html', range(1, 16)),
    ('https://mcstories.com/BabyStepsTrancingEmily/BabyStepsTrancingEmily{}.html', range(1, 9)),
    ('https://mcstories.com/BabyStepsTrainingEmily/BabyStepsTrainingEmily1.html', range(1, 9)),
    ('https://mcstories.com/BabyStepsAlternateEnding/BabyStepsAlternateEnding1.html', range(1,3)),
    ('https://mcstories.com/Bunker/Bunker2.html', range(2,4)),
    ('https://mcstories.com/Influencer/Influencer1.html', range(2,21)),
    ('https://mcstories.com/RoleReversalMindSpark/RoleReversalMindSpark1.html', range(2,8)),
]

# Open the result file for writing
with open('train.csv', 'w') as result_file:
    result_file.write('text,\n')
    # Iterate over each base URL and its corresponding page number range
    for base_url, page_numbers in base_urls:
        # Iterate over each page number
        for page_number in page_numbers:
            # Set the URL of the current page to scrape
            url = base_url.format(page_number)

            # Send a GET request to the URL and get the response
            response = requests.get(url)

            # Parse the HTML response using BeautifulSoup
            soup = BeautifulSoup(response.text, 'html.parser')

            # Find the <article> element with id="mcstories"
            article_element = soup.find('article', {'id': 'mcstories'})

            # Find all <section> elements within the <article> element that are not inside a <footer> element
            sections = article_element.find_all('section', recursive=False)

            # Iterate over each <section> element
            for section in sections:
                # Check if the <section> element is inside a <footer> element
                if section.find_parents('footer'):
                    # Skip this <section> element if it is inside a <footer> element
                    continue
                # Find all <p> elements within the <section> element
                paragraphs = section.find_all('p')
                # Write each paragraph to the result file as a separate line
                for paragraph in paragraphs:
                    # Remove any newline characters from the extracted text and replace them with spaces
                    text = paragraph.text.replace('\n', ' ')
                    result_file.write('"' + text + '",\n')
                    # result_file.write(text + '\n')
