#%% import packages
from transformers import pipeline

## classification
classifier = pipeline('sentiment-analysis')
input1 = 'I\'ve been waiting for HuggingFace course my whole life.'
classifier(input1)
input2 =  'I woke up in the morning today with a headache'
classifier([input1, input2])

#%% zero-shot classification
zs_classifier = pipeline('zero-shot-classification')
# input4 = 'This is a course about Transformers library'
# input4 = 'Wizard was standing on the hilltop when a magical beam of energy shot through the sky and took down one of the pillars king\'s castle'
# input4 = 'Gerald re-attached his bionic arm before checking how much damage the starship had done to his temporary home at the new planet'
# input4 = 'Clara couldn\'t believe that the dismembered arm was still moving, blood gushing out from the severed end'
poem = 'Beneath the sky\'s expansive blue, A field of flowers bathed in dew.  Butterflies dance in the light, Their wings aflutter, pure and bright.  In this moment, all feels right, Underneath the sun so bright.  Underneath the silver moon, whispers of love will be heard soon.'
zs_classifier(
  poem,
  candidate_labels=['robots', 'news', 'poem', 'space']
)

#%% feature extraction
feature_extracter = pipeline('feature-extraction')
vector = feature_extracter('The answer to Life, Universe and Everything')
print(f'Number of sentences: {len(vector)}')
print(f'Number of tokens in first sentence: {len(vector[0])}')
print(f'Number of floats in first token: {len(vector[0][0])}')
print(f'first few floats in first token: {vector[0][0][:5]}')

#%% text generation
generator = pipeline('text-generation')
input3 = 'I woke up in the morning with a massive headache and'
# generator(input3)
generator(input3, num_return_sequences=2, max_length=15)

#%% custom model in a pipeline
gpt2 = pipeline('text-generation', model='distilgpt2')
generator(
  'In this course, we will teach you how to',
  max_length=30,
  num_return_sequences=2
)

#%% bloom model supports 48 languages
bloom_560m = pipeline('text-generation', model='bigscience/bloom-560m')
generator('मेरा नाम बिंग है और मैं आपकी सहायता '), generator('mujhe subah uthkar gaana gaane ki')

#%% mask filling
unmasker = pipeline('fill-mask', model='bert-base-cased')
unmasker('This [MASK] will teach you all about [MASK] models', top_k=2)

#%% named entity recognition
ner = pipeline('ner', grouped_entities=True)
ner('My name is Sylvania and I work at Hugging Face in Brooklyn')
ner('John Smith, while living in London, wrote the book ‘The Mysteries of the Universe’, which was published by Penguin Random House.')

#%% question answering
responder = pipeline('question-answering')
responder(question='What is the capital of France?',
          context='France is a European country whose capital city is Paris and it raises a capital of 765 euros per annum')

#%% summarisation
summarise = pipeline('summarization')
summarise(
    """
    America has changed dramatically during recent years. Not only has the number of
    graduates in traditional engineering disciplines such as mechanical, civil,
    electrical, chemical, and aeronautical engineering declined, but in most of
    the premier American universities engineering curricula now concentrate on
    and encourage largely the study of engineering science. As a result, there
    are declining offerings in engineering subjects dealing with infrastructure,
    the environment, and related issues, and greater concentration on high
    technology subjects, largely supporting increasingly complex scientific
    developments. While the latter is important, it should not be at the expense
    of more traditional engineering.

    Rapidly developing economies such as China and India, as well as other
    industrial countries in Europe and Asia, continue to encourage and advance
    the teaching of engineering. Both China and India, respectively, graduate
    six and eight times as many traditional engineers as does the United States.
    Other industrial countries at minimum maintain their output, while America
    suffers an increasingly serious decline in the number of engineering graduates
    and a lack of well-educated engineers.
"""
)

summarise(
"""
As first reported by Cricbuzz, the ICC have announced a 34,000-seater pop-up facility about 30 miles east of New York City as one of venues for the much-anticipated 2024 T20 World Cup co-hosted by the USA and the West Indies. The facility will be erected in the Eisenhower Park spread across 930 acres in East Meadow, a hamlet on Long Island, about 30 miles east of Manhattan. It is understood that the venue will also play host to the marquee clash between India and Pakistan.

The announcement comes within months after the substantive talks between ICC and New York City officials fell through for the construction of a similar pop venue in Van Cortlandt Park in the borough of Bronx. The city officials were compelled to drop the plans for Bronx after some heavy resistance from some of the locals residing around the park and one of the cricket leagues that is based out of the same park. Bronx's loss was Nassau County's gain as the ICC proved to be quick on its feet to materialize negotiations with the Nassau County officials, the administrators of the Eisenhower Park.

There has been considerable clamor within the ICC to have cricket make headway in USA. The country provides a two-pronged appeal being the world's biggest media market as well as the fastest-emerging cricket market around the globe. The USA media rights for ICC events are believed to be amongst the top 4 countries in terms of dollar value. The designation of USA as co-hosts for the T20 World Cup was one such affirmative action from the ICC.
"""
)

#%% translation
translater = pipeline('translation', model='Helsinki-NLP/opus-mt-fr-en')
# translater('Ce cours est produit par Hugging Face')

hindi_translator = pipeline('translation', model='facebook/nllb-200-distilled-600M')
hindi_translator('I have a hard time learning about large language models',
                 src_lang='eng_Latn', tgt_lang='hin_Deva')
