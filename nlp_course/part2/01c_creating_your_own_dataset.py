#%% import packages
# !pip install requests
# Code of your application, which uses environment variables (e.g. from `os.environ` or
# `os.getenv`) as if they came from the actual environment.
import datetime
import math
import os
import pprint
import time
from pathlib import Path

import pandas as pd
import requests
from datasets import load_dataset
from dotenv import load_dotenv
from tqdm.notebook import tqdm

load_dotenv()
token = os.getenv('GITHUB_TOKEN')
headers = {'Authorization': f'token {token}'}
pp = pprint.PrettyPrinter(indent=2).pprint

url = 'https://api.github.com/repos/huggingface/datasets/issues?page=1&per_page=1'
response = requests.get(url)

#%%
print('status code:', response.status_code)
pp(type(response))
print('response dict')
pp(response.__dict__)
print('response json:')
pp(response.json())

def print_object(obj):
    for attr in dir(obj):
        if not attr.startswith('__') and not callable(getattr(obj, attr)):
            print(f'{attr}: {getattr(obj, attr)}')
print_object(response)

# %% download all the issues from a Github repo
# another way to wait an hour that works even if the computer goes to suspend mode: # sleep_for(60)
def sleep_for(mins=60):
  target_time = (datetime.datetime.now() + datetime.timedelta(minutes=mins)).time()
  while True:
      current_time = datetime.datetime.now().time()
      if current_time >= target_time:
          break
      time.sleep(1)

def fetch_issues(
  owner='huggingface',
  repo='datasets',
  num_issues=10_000,
  rate_limit=5_000,
  issues_path=Path('.')
):
  if not issues_path.is_dir():
    issues_path.mkdir(exist_ok=True)

  batch = []
  all_issues = []
  per_page = 100
  num_pages = math.ceil(num_issues / per_page)
  base_url = 'http://api.github.com/repos'

  for page in tqdm(range(num_pages)):
    query = f'issues?page={page}&per_page={per_page}&state=all'
    issues = requests.get(f'{base_url}/{owner}/{repo}/{query}', headers=headers)
    batch.extend(issues.json())
    print('page:', page, 'len of batch:', len(batch))

    # if len(batch) > rate_limit and len(all_issues) < num_issues:
      # all_issues.extend(batch)
      # batch = []
      # print(f'Reached Github rate limit. Sleeping for one hour...')
      # time.sleep(60 * 60 + 1)
      # sleep_for(60)

  all_issues.extend(batch)
  print('len of all_issues:', len(all_issues))
  # df = pd.DataFrame.from_records(all_issues)
  df = pd.DataFrame.from_records(all_issues).dropna(axis=1, how='all').drop(['milestone'], axis=1)
  save_path = f'{issues_path}/{repo}-issues.jsonl'
  df.to_json(save_path, orient='records', lines=True)
  print( f'Downloaded all the issues for {repo}! Dataset stored at {save_path}')

fetch_issues()

# %% create column 'is_pull_request'
issues_ds = load_dataset('json', data_files='datasets-issues.jsonl', split='train')
print('issues dataset:', issues_ds)

sample = issues_ds.shuffle(seed=666).select(range(3))

for url, pr in zip(sample['html_url'], sample['pull_request']):
  print('URL:', url)
  print('\nPull request:', pr)

issues_ds = issues_ds.map(
  lambda x: {'is_pull_request': False if x['pull_request'] is None else True}
)
print('\nissues dataset:', issues_ds)

#%% exercise: Calculate the average time it takes to close issues in 🤗 Datasets.
# You may find the Dataset.filter() function useful to filter out the pull requests and open issues,
# and you can use the Dataset.set_format() function to convert the dataset to a DataFrame so
# you can easily manipulate the created_at and closed_at timestamps. For bonus points, calculate
# the average time it takes to close pull requests.

#%% fetch comments associated with an issue number
issue_number = 2792
url = f'https://api.github.com/repos/huggingface/datasets/issues/{issue_number}/comments'
response = requests.get(url, headers=headers)
response.json()

## return all the comments associated with an issue
def get_comments(issue_number):
  url = f'https://api.github.com/repos/huggingface/datasets/issues/{issue_number}/comments'
  response = requests.get(url, headers=headers)
  return [r['body'] for r in response.json()]

for c in get_comments(issue_number):
  print('\n=====Comment=====\n', c)

issues_ds_with_comments = issues_ds.map(lambda x: {'comments': get_comments(x['number'])})
print('\nissues dataset with comments:', issues_ds_with_comments)

#%%
import os

os.getcwd()
