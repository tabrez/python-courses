#%% import packages
import logging

from accelerate import Accelerator
from datasets import load_dataset
from torch.utils.data import DataLoader
from tqdm.auto import tqdm
from transformers import (AdamW, AutoModelForSequenceClassification,
                          AutoTokenizer, DataCollatorWithPadding,
                          get_scheduler)

logging.getLogger().setLevel(logging.ERROR)

#%% code we need from 03_finetuning_a_pretrained_model.py
raw_ds = load_dataset('glue', 'mrpc')
checkpoint = 'bert-base-uncased'
tokenizer = AutoTokenizer.from_pretrained(checkpoint)

def tokenize_function(sample):
  return tokenizer(sample['sentence1'], sample['sentence2'], truncation=True)

tokenized_ds = raw_ds.map(tokenize_function, batched=True)
data_collator = DataCollatorWithPadding(tokenizer=tokenizer)

#%% preparing for  training
tokenized_ds = tokenized_ds.remove_columns(['sentence1', 'sentence2', 'idx'])
tokenized_ds = tokenized_ds.rename_column('label', 'labels')
tokenized_ds.set_format('torch')
print('column names:', tokenized_ds['train'].column_names)

#%% create dataloaders
train_dl = DataLoader(tokenized_ds['train'], shuffle=True, batch_size=8, collate_fn=data_collator)
eval_dl = DataLoader(tokenized_ds['validation'], batch_size=8, collate_fn=data_collator)

#%% training loop with `accelerate`
accelerator = Accelerator()
model = AutoModelForSequenceClassification.from_pretrained(checkpoint)
optimizer = AdamW(model.parameters(), lr=3e-5)

train_dl, eval_dl, model, optimizer = accelerator.prepare(
  train_dl, eval_dl, model, optimizer
)

num_epochs = 3
num_training_steps = num_epochs * len(train_dl)
lr_scheduler = get_scheduler(
  'linear',
  optimizer=optimizer,
  num_warmup_steps=0,
  num_training_steps=num_training_steps
)

progress_bar = tqdm(range(num_training_steps))

model.train()
outputs = None
for epoch in range(num_epochs):
  for batch in train_dl:
    outputs = model(**batch)
    loss = outputs.loss
    accelerator.backward(loss)

    optimizer.step()
    lr_scheduler.step()
    optimizer.zero_grad()
    progress_bar.update(1)

print('loss:', outputs.loss)
