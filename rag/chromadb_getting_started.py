## https://docs.trychroma.com/getting-started
## https://python.langchain.com/docs/modules/data_connection/document_transformers/

import glob
import os

#%% import packages
#!pip install chromadb
import chromadb
from langchain.text_splitter import RecursiveCharacterTextSplitter

splitter = RecursiveCharacterTextSplitter(
  chunk_size=2000,
  chunk_overlap=200,
  length_function=len,
  add_start_index=True
)

documents = []
metadatas = []
ids = []
paths = glob.glob(os.path.join('data', 'file*.txt'))

for i, file_path in enumerate(paths):
  with open(file_path, 'r') as f:
    chunks = splitter.create_documents([f.read()])
    for j, chunk in enumerate(chunks):
      documents.append(chunk.page_content)
      metadatas.append({'filename': file_path})
      ids.append(f'${i}:${j}')

chroma_client = chromadb.Client()
collection_name = 'cricket_players'
if collection_name in chroma_client.list_collections():
  chroma_client.delete_collection(name=collection_name)
collection = chroma_client.create_collection(name=collection_name)

collection.add(
  documents=documents,
  metadatas=metadatas,
  ids=ids
)

print('embeddings added to chromadb collection')

#%%
results = collection.query(
  query_texts=['Who is Dravid?'],
  n_results=2
)

print('results:', results)
