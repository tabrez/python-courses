#%% using pipeline
from transformers import pipeline

classifier = pipeline('sentiment-analysis')
classifier(
  [
    'I\'ve been waiting for a HuggingFace course my whole life.',
    'I hate this so much'
  ]
)

#%% create the tokenizer
from transformers import AutoTokenizer

checkpoint = 'distilbert-base-uncased-finetuned-sst-2-english'
tokenizer = AutoTokenizer.from_pretrained(checkpoint)

raw_inputs = [
    'I\'ve been waiting for a HuggingFace course my whole life.',
    'I hate this so much'
]
inputs = tokenizer(raw_inputs, padding=True, truncation=True, return_tensors='pt')
print(inputs)

#%% create the model
from transformers import AutoModel

checkpoint = 'distilbert-base-uncased-finetuned-sst-2-english'
model = AutoModel.from_pretrained(checkpoint)
print(f'model architecture: {model}')

outputs = model(**inputs)
print(f'outputs shape: {outputs.last_hidden_state.shape}')
print(f'outputs: {outputs}')

#%% create a classification model
from transformers import AutoModelForSequenceClassification
import torch

checkpoint = 'distilbert-base-uncased-finetuned-sst-2-english'
model = AutoModelForSequenceClassification.from_pretrained(checkpoint)
outputs = model(**inputs)

print(f'outputs: {outputs.keys()}')
print(outputs.logits.shape)
print(outputs.logits)

predictions = torch.nn.functional.softmax(outputs.logits, dim=-1)
print(predictions)

print(f'labels: {model.config.id2label}')


#%% closer look at models
from transformers import BertConfig, BertModel

config = BertConfig()
print(f'model config: {config}')
# following method creates an untrained model(random weights)
model = BertModel(config)
# print(f'model architecture: {model}')

#%% save a model
model.save_pretrained('output')
#> ls output
# config.json pytorch_model.bin

#%% closer look at tokenizer
from transformers import BertTokenizer

tokenizer = BertTokenizer.from_pretrained('bert-base-cased')
# tokenizer = AutoTokenizer.from_pretrained('bert-base-cased')

seq = 'Using a Transformer network is simple'
tokenizer(seq)
# tokenizer.save_pretrained('output')

tokens = tokenizer.tokenize(seq)
print(f'tokens: {tokens}')
ids = tokenizer.convert_tokens_to_ids(tokens)
print(f'ids: {ids}')
# convert the above ids to tensors and feed them to the model

decoded = tokenizer.decode(ids)
print(f'decoded string: {decoded}')

#%% handling multiple sequences
checkpoint = 'distilbert-base-uncased-finetuned-sst-2-english'
tokenizer = AutoTokenizer.from_pretrained(checkpoint)
model = AutoModelForSequenceClassification.from_pretrained(checkpoint)

seq = 'I\'ve been waiting for a HuggingFace course my whole life.'

tokens = tokenizer.tokenize(seq)
ids = tokenizer.convert_tokens_to_ids(tokens)
# inputs = torch.tensor(ids)
inputs = torch.tensor([ids, ids])
print('Input ids:', inputs)

output = model(inputs)
print('Logits:', output.logits)

#%% padding
seq1_ids = [[200, 200, 200]]
seq2_ids = [[200, 200]]
batched_seq_ids = [
  [200, 200, 200],
  [200, 200, tokenizer.pad_token_id]
]

print(model(torch.tensor(seq1_ids)).logits)
print(model(torch.tensor(seq2_ids)).logits)
## results are not the same for the second sequence in the batch as the model
## also takes the padding token into account
print(model(torch.tensor(batched_seq_ids)).logits)

attention_mask = [
  [1, 1, 1],
  [1, 1, 0]
]

outputs = model(torch.tensor(batched_seq_ids), attention_mask=torch.tensor(attention_mask))
print('output logits:', outputs.logits)

# seq = seq[:max_sequence_length]

#%% putting it all together
sequences = ['I\'ve been waiting for a HuggingFace course my whole life.', "But not really."]
# padding = 'longest' | padding = 'max_length' & max_length = 25
# sequences that are longer than max_length are truncated
# sequences that are shorter than max_length are padded
model_inputs = tokenizer(sequences,
                         padding='longest',
                         truncation=True,
                         max_length=20,
                         return_tensors='pt')
print('inputs:', model_inputs)
output = model(**model_inputs)
print('model output:', output)
print('ids to string:', tokenizer.decode(ids))
input = tokenizer(seq)
print('ids to string after tokenising:', tokenizer.decode(input['input_ids']))
