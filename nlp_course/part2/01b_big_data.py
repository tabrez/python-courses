#%% import packages
from datasets import load_dataset

# !pip install zstandard
%cd part2
data_files = 'https://the-eye.eu/public/AI/pile_preliminary_components/PUBMED_title_abstracts_2019_baseline.jsonl.zst'
data_files = 'https://mystic.the-eye.eu/public/AI/pile_preliminary_components/PUBMED_title_abstracts_2019_baseline.jsonl.zst'
pubmed_ds = load_dataset('json', data_files=data_files, split='train')
print('pubmed_ds:', pubmed_ds)

# %%
