# %% import packages

import evaluate
import torch
from datasets import load_dataset
from torch.utils.data import DataLoader
from transformers import (
  AutoModelForSequenceClassification,
  AutoTokenizer,
  Trainer,
  TrainingArguments,
  pipeline,
)

output_dir = '/home/tabrez/.huggingface/results/bert-emotions'


# %% load emotions dataset, create the tokenizer & tokenize the dataset
def get_data(path, split=None):
  return load_dataset(path) if split is None else load_dataset(path, split=split)


def get_tokenized_data(data, tokenizer):
  def tokenize_batch(batch):
    return tokenizer(batch['text'], max_length=512, truncation=True, padding=True)

  return data.map(tokenize_batch, batched=True)


model_name = 'bert-base-uncased'
data_path = 'dair-ai/emotion'
tokenizer = AutoTokenizer.from_pretrained(model_name)
model = AutoModelForSequenceClassification.from_pretrained(model_name, num_labels=6)
data = get_data(data_path)
print('raw dataset:', data)
ds = get_tokenized_data(data, tokenizer)
print('tokenized dataset:', ds)

# %% create and train the model
training_args = TrainingArguments(
  output_dir=output_dir,
  evaluation_strategy='epoch',
  logging_strategy='epoch',
  save_strategy='epoch',
  per_device_train_batch_size=64,
  # push_to_hub=True,
)

trainer = Trainer(
  model=model,
  args=training_args,
  train_dataset=ds['train'].select(range(128)),
  eval_dataset=ds['validation'].select(range(128)),
  tokenizer=tokenizer,
)

trainer.train()

# %% calculate loss on test data using trainer object
def get_metric(metric, predictions, references):
  metric = evaluate.load(metric)
  return metric.compute(
    predictions=predictions,
    references=references,
  )


# ds = get_tokenized_data()
output = trainer.predict(ds['test'])
predictions = torch.tensor(output.predictions).argmax(dim=-1)
results = get_metric('accuracy', predictions, ds['test']['label'])
print('before training: ', results)


trainer.train()


# ds = get_tokenized_data()
output = trainer.predict(ds['test'])
predictions = torch.tensor(output.predictions).argmax(dim=-1)
results = get_metric('accuracy', predictions, ds['test']['label'])
print('after training: ', results)


# %% get predictions for the test dataset
def get_predictions(ds, model):
  def collate_fn(batch):
    return {key: torch.tensor([d[key] for d in batch]) for key in batch[0]}

  dataloader = DataLoader(ds, batch_size=16, collate_fn=collate_fn)

  outputs = {'logits': []}
  with torch.no_grad():
    for batch in dataloader:
      output = model(**batch)
      outputs['logits'].extend((torch.unbind(output.logits)))

  outputs = torch.stack(outputs['logits'])
  predictions = outputs.argmax(dim=-1)
  print('\nlogits argmax:', predictions)

  return predictions


model_path = f'{output_dir}/checkpoint-6000'
# model_path = 'itabrez/bert-emotions'
model = AutoModelForSequenceClassification.from_pretrained(model_path)
ds_collate = ds.remove_columns(['text', 'label'])
predictions = get_predictions(ds_collate['test'], model)
results = get_metric('accuracy', predictions, ds['test']['label'])
print(results)


# %% calculate accuracy, precision, recall & f1 score for the test dataset using
# `metric.compute`
def get_evaluation_results(predictions, references):
  results = []

  metric = evaluate.load('accuracy')
  result = metric.compute(predictions=predictions, references=references)
  results.append(result)

  metrics = [evaluate.load('precision'), evaluate.load('recall'), evaluate.load('f1')]
  for metric in metrics:
    result = metric.compute(
      predictions=predictions, references=references, average='weighted'
    )
    results.append(result)
  return results


data = get_data(data_path, 'test')
results = get_evaluation_results(predictions, data['test']['label'])
for res in results:
  print(res)


# %% calculate accuracy metric using `evaluate.compute` and `pipeline`
def get_evaluation_results_pipeline():
  task_evaluator = evaluate.evaluator('text-classification')
  pipe = pipeline('text-classification', model=model_path)
  eval_results = task_evaluator.compute(
    model_or_pipeline=pipe,
    data=get_data(),
    label_mapping={
      'LABEL_0': 0,
      'LABEL_1': 1,
      'LABEL_2': 2,
      'LABEL_3': 3,
      'LABEL_4': 4,
      'LABEL_5': 5,
    },
  )
  return eval_results


eval_results = get_evaluation_results_pipeline
print('evaluation results:', eval_results)
