# %% import packages
import evaluate
import pandas as pd
from datasets import load_dataset
from evaluate import evaluator
from evaluate.visualization import radar_plot
from transformers import AutoModelForSequenceClassification, pipeline

# %% Evaluate models on the hub
data = load_dataset('imdb', split='test').shuffle(seed=42).select(range(1000))
task_evaluator = evaluator('text-classification')
model_name = 'lvwerra/distilbert-imdb'

# %% 1. Pass a model name or path
eval_results = task_evaluator.compute(
  model_or_pipeline=model_name,
  data=data,
  label_mapping={'NEGATIVE': 0, 'POSITIVE': 1},
)
print('evaluation results using model name:', eval_results)

# %% 2. Pass an instantiated model; doesn't work: 'Impossible to guess which tokenizer to use.'
model = AutoModelForSequenceClassification.from_pretrained(model_name)
eval_results = task_evaluator.compute(
  model_or_pipeline=model,
  data=data,
  label_mapping={'NEGATIVE': 0, 'POSITIVE': 1},
)
print('evaluation results using instantiated model:', eval_results)

# %% 3. Pass an instantiated pipeline
pipe = pipeline('text-classification', model=model_name)
eval_results = task_evaluator.compute(
  model_or_pipeline=pipe,
  data=data,
  label_mapping={'NEGATIVE': 0, 'POSITIVE': 1},
)
print('evaluation results using pipeline:', eval_results)

# %% evaluate multiple metrics
eval_results = task_evaluator.compute(
  model_or_pipeline=model_name,
  data=data,
  metric=evaluate.combine(['accuracy', 'recall', 'precision', 'f1']),
  label_mapping={'NEGATIVE': 0, 'POSITIVE': 1},
)
print('evaluation of multiple metrics:', eval_results)

# %% benchmarking several models

models = [
  'xlm-roberta-large-finetuned-conll03-english',
  'dbmdz/bert-large-cased-finetuned-conll03-english',
  'elastic/distilbert-base-uncased-finetuned-conll03-english',
  'dbmdz/electra-large-discriminator-finetuned-conll03-english',
  'gunghio/distilbert-base-multilingual-cased-finetuned-conll2003-ner',
  'philschmid/distilroberta-base-ner-conll2003',
  'Jorgeutd/albert-base-v2-finetuned-ner',
]

data = load_dataset('conll2003', split='validation')
data = data.shuffle(seed=42).select(range(1000))
task_evaluator = evaluator('token-classification')

results = []
for model in models:
  res = task_evaluator.compute(model_or_pipeline=model, data=data, metric='seqeval')
  results.append(res)
df = pd.DataFrame(results, index=models)
# print(f'results for {len(models)} modelss:', df)
df = df[
  [
    'overall_f1',
    'overall_accuracy',
    'total_time_in_seconds',
    'samples_per_second',
    'latency_in_seconds',
  ]
]
print(f'overall results for {len(models)} modelss:', df)

# %% Visualising the results
plot = radar_plot(data=results, model_names=models, invert_range=['latency_in_seconds'])
plot.show()
plot.savefig(bbox_inches='tight')

# %% Question answering
task_evaluator = evaluator('question-answering')

data = load_dataset('squad', split='validation[:1000]')
eval_results = task_evaluator.compute(
  model_or_pipeline='distilbert-base-uncased-distilled-squad',
  data=data,
  metric='squad',
  strategy='bootstrap',
  n_resamples=30,
)
print('results of question-answering evaluation task:', pd.DataFrame(eval_results))

# %% Image classification: didn't execute as it requires download of 150GB files
data = load_dataset('imagenet-1k', split='validation', use_auth_token=True)

pipe = pipeline(
  task='image-classification', model='facebook/deit-small-distilled-patch16-224'
)

task_evaluator = evaluator('image-classification')
eval_results = task_evaluator.compute(
  model_or_pipeline=pipe,
  data=data,
  metric='accuracy',
  label_mapping=pipe.model.config.label2id,
)
print('results of evaluating model on imagenet dataset:', eval_results)
