# %% import packages
import gc
import time

import torch
from datasets import load_dataset
from transformers import (
  AutoModelForSeq2SeqLM,
  AutoTokenizer,
  DataCollatorForSeq2Seq,
  Seq2SeqTrainer,
  Seq2SeqTrainingArguments,
)


def time_function(func):
  def wrapper(*args, **kwargs):
    start = time.time()
    result = func(*args, **kwargs)
    elapsed_time = time.time() - start

    if elapsed_time < 60:
      print(f'Executed time: {elapsed_time:.2f} secs')
    else:
      print(f'Executed time: {elapsed_time / 60:.2f} mins')
    return result

  return wrapper


def format_memory(size_in_bytes):
  for unit in ['bytes', 'KB', 'MB', 'GB']:
    if size_in_bytes < 1024 or unit == 'GB':  # No need to go beyond GB
      break
    size_in_bytes /= 1024
  return f'{size_in_bytes:.2f} {unit}'


# def format_memory(size_in_bytes):
#   if size_in_bytes < 1024:
#     return f'{size_in_bytes} bytes'
#   elif size_in_bytes < (1024**2):  # less than 1MB = 1024 * 1024
#     return f'{size_in_bytes / 1024:.2f} KB'
#   elif size_in_bytes < 1024**3:
#     return f'{size_in_bytes / 1024 / 1024:.2f} MB'
#   else:
#     return f'{size_in_bytes / 1024 / 1024 / 1024:.2f} GB'


def clear_cuda_cache():
  mem = torch.cuda.memory_allocated()
  max_mem = torch.cuda.max_memory_allocated()

  print('Before:')
  print('Allocated memory:', format_memory(mem))
  print('Max allocated memory:', format_memory(max_mem))

  if torch.cuda.is_available():
    print('emptying cache...')
    torch.cuda.empty_cache()
  gc.collect()
  print('\nAfter:')
  mem = torch.cuda.memory_allocated()
  max_mem = torch.cuda.max_memory_allocated()

  print('Allocated memory:', format_memory(mem))
  print('Max allocated memory:', format_memory(max_mem))


# %% Call the function
# clear_cuda_cache()

# %% t5-small supports translation from English to German, French & Romanian
# TODO: max_length is all over the place in this program, find the correct value
model_path = 't5-small'
tokenizer = AutoTokenizer.from_pretrained(model_path, model_max_length=512)
model = AutoModelForSeq2SeqLM.from_pretrained(model_path, max_length=512)


def get_translation(model, tokenizer, input_text):
  input_ids = tokenizer.encode(
    input_text, max_length=300, truncation=True, return_tensors='pt'
  )
  outputs = model.generate(input_ids)
  return tokenizer.decode(outputs[0])


# %% translation from English to Hungarian is not supported, following might produce
# translation to french, german or romanian
input_text = 'translate English to Hungarian: Studies have been shown that owning a dog is good for you'
decoded_output = get_translation(model, tokenizer, input_text)
print(decoded_output)
# TODO: use huggingface pipeline to detect the language of `decoded_output`

# %% load the english to hungarian tranlsation dataset
raw_data = load_dataset('opus_books', 'en-hu')
raw_data = raw_data['train'].train_test_split(test_size=0.2)
print('raw data:', raw_data)
print('frist of raw data:', raw_data['train'][0])


# %% pre-process data to move 'en' and 'hu' fields from 'translation' dict to columns
# TODO: combine to following two functions
@time_function
def tokenize_dataset(raw_data):
  def tokenize_batch(batch):
    # extract input and targets from 'translation' dict
    prefix = 'translate English to Hungarian: '
    inputs = [prefix + sample['en'] for sample in batch['translation']]
    targets = [sample['hu'] for sample in batch['translation']]

    return tokenizer(
      inputs,
      text_target=targets,
      max_length=512,
      truncation=True,
    )

  return raw_data.map(tokenize_batch, batched=True)


ds = tokenize_dataset(raw_data)
print(ds)
print('first ds:', ds['train'][0])

# %%
output_dir = '/home/tabrez/.huggingface/results/t5-small-opus-books'
training_args = Seq2SeqTrainingArguments(
  output_dir=output_dir,
  evaluation_strategy='epoch',
  logging_strategy='epoch',
  per_device_train_batch_size=20,
  per_device_eval_batch_size=20,
  save_strategy='epoch',
  predict_with_generate=True,
  # num_train_epochs=5,
  # push_to_hub=True,
)
data_collator = DataCollatorForSeq2Seq(
  tokenizer=tokenizer,
  model=model_path,
)
trainer = Seq2SeqTrainer(
  model=model,
  args=training_args,
  train_dataset=ds['train'],
  eval_dataset=ds['test'],
  tokenizer=tokenizer,
  data_collator=data_collator,
)

# %%
# outputs_test = trainer.predict(ds['test'])
# print('shape of outputs_test.predictions:', outputs_test.predictions.shape)
# print('len of first of ds labels:', len(ds['test'][0]['labels']))
# print('raw data:', raw_data)

# %%
time_function(trainer.train)()

# %% load model from the disk and use it for inference on one sample
# model_path = output_dir + '/checkpoint-41145'
# tokenizer = AutoTokenizer.from_pretrained(model_path, model_max_length=512)
# model = AutoModelForSeq2SeqLM.from_pretrained(model_path, max_length=512)
model.to('cpu')
input_text = 'translate English to Hungarian: Studies have been shown that owning a dog is good for you'
input_text = 'translate English to Hungarian: I woke up in the morning today with a splitting headache'
decoded_output = get_translation(model, tokenizer, input_text)
print(decoded_output)


# %%
# print('len of ds labels:', len(ds['test']['labels'][0]))
print('output dir:', trainer._get_output_dir(trial='2'))
