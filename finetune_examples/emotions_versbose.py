# %% import packages
import time

import evaluate
import torch
from datasets import load_dataset
from torch.utils.data import DataLoader
from transformers import (
  AutoModelForSequenceClassification,
  AutoTokenizer,
  # DataCollatorWithPadding,
  Trainer,
  TrainingArguments,
  pipeline,
)

# %% load emotions dataset and inspect it
data = load_dataset('dair-ai/emotion')
print(data)


# %% create the tokenizer & tokenize the dataset
def create_tokenize_batch(tokenizer):
  _debug = True

  def tokenize_batch(batch):
    nonlocal _debug
    if _debug:
      print('type of batch:', type(batch))
      print('len of batch:', len(batch['text']))
      # print('keys of batch:', batch.keys())
      print('batch:', batch['text'][:3])
      _debug = False
    return tokenizer(batch['text'], max_length=512, truncation=True)

  return tokenize_batch


model_name: str = 'bert-base-uncased'
tokenizer = AutoTokenizer.from_pretrained(model_name)
tokenize_batch = create_tokenize_batch(tokenizer)
ds = data.map(tokenize_batch, batched=True)
print(ds)

# %% create and train the model
model = AutoModelForSequenceClassification.from_pretrained(model_name, num_labels=6)
# data_collator = DataCollatorWithPadding(tokenizer=tokenizer, max_length=512)

training_args = TrainingArguments(
  output_dir='/home/tabrez/.huggingface/results/bert-emotions',
  evaluation_strategy='epoch',
  logging_strategy='epoch',
  save_strategy='epoch',
  push_to_hub=True,
)

# iterate on subset of dataset
# size = 24
# ds_mini = {}
# ds_mini['train'] = ds['train'].shuffle(seed=42).select(range(size))
# ds_mini['validation'] = ds['validation'].select(range(size))

trainer = Trainer(
  model=model,
  args=training_args,
  train_dataset=ds['train'],
  eval_dataset=ds['validation'],
  tokenizer=tokenizer,
  # data_collator=data_collator,
)

start = time.time()
trainer.train()
end = time.time()
print('execution time:', (end - start) / 60, ' mins')

# %% calculate loss on test data using trainer object
predictions = trainer.predict(ds['test'])

# %% load and tokenize the test dataset
data = load_dataset('dair-ai/emotion', split='test')
print('data:', data)

model_path = '/home/tabrez/.huggingface/results/bert-emotions/checkpoint-6000'
# model_path = 'itabrez/bert-emotions'
tokenizer = AutoTokenizer.from_pretrained(model_path)
model = AutoModelForSequenceClassification.from_pretrained(model_path)
print(model)


# TODO: error suggests we have to use padding in the following call to use
# return_tensors argument(which is needed to make model(**ds) work correctly)
# how is this different from doing padding using DataCollatorWithPadding during training?
# https://discuss.huggingface.co/t/dataset-map-return-only-list-instead-torch-tensors/15767/4
def tokenize_batch(batch):
  return tokenizer(
    batch['text'], max_length=512, truncation=True, padding=True, return_tensors='pt'
  )


ds = data.map(tokenize_batch, batched=True)
# the following labels are needed for `trainer.train()` to work but they need to be
# absent for `collate_fn` to work as we are converting all columns to tensors;
# better way would be to only convert `input_ids` and `attension_mask` to tensors
ds = ds.remove_columns(['text', 'label'])
ds.set_format('pt', columns=['input_ids', 'attention_mask'], output_all_columns=True)
print('type of input_ids:', type(ds['input_ids']))
print('type of attention_mask:', type(ds['attention_mask']))
print('ds:', ds)
print('\nfirst of ds.input_ids:', ds['input_ids'][0])

# %% model inference on a single sentence
text = 'The music festival was a grand success, people enjoyed it a lot.'
text = (
  'I was charged such a huge fee for the show and we did not even get a good seat, that is outrageous.',
)
text = 'Her grandmother passed away way too soon'
text = 'I was sure I was going to die when I saw the bear running towards me'
inputs = tokenizer(text, return_tensors='pt')
print('inputs:', inputs)
predictions = model(**inputs)
print('\nlogits argmax:', predictions.logits.argmax())


# %% get predictions for the entire test dataset
# The `map()` function strips the returned tokenized ids from tensor wrapper even if
# we use `return_tensors='pt'` in the `tokenizer()` function call. We wrap the numbers
# in tensors using `collate_fn`
def collate_fn(batch):
  return {key: torch.tensor([d[key] for d in batch]) for key in batch[0]}


# ds_mini = ds.select(range(32))
dataloader = DataLoader(ds, batch_size=16, collate_fn=collate_fn)

# alternatively:
# ds = ds.set_format("pt", columns=["input_ids"], output_all_columns=True)

# TODO: use a simple list `outputs = []` instead
outputs = {'logits': []}
with torch.no_grad():
  _debug = True
  for batch in dataloader:
    output = model(**batch)
    if _debug:
      print('batch:', batch)
      print('keys of batch:', batch.keys())
      print('shape of batch.input_ids:', batch['input_ids'].shape)
      print('output:', output)
      print('type of output:', type(output))
      print('len of output:', len(output))
      print('keys of output:', output.keys())
      print('shape of output.logits:', output.logits.shape)
      _debug = False
    outputs['logits'].extend((torch.unbind(output.logits)))

# %% extract the predictions and print them
# print('type of outputs:', outputs)
print('keys of outputs:', outputs.keys())
print('\nlen of prediction logits:', len(outputs['logits']))
print('\nfirst of prediction logits:', outputs['logits'][0])
outputs = torch.stack(outputs['logits'])
print('\nshape of prediction logits:', outputs.shape)
predictions = outputs.argmax(dim=-1)
print('\nlogits argmax:', predictions)

# %% cleaned up code from above cell: no batch loader, no print statements
with torch.no_grad():
  outputs = model(input_ids=ds['input_ids'], attention_mask=ds['attention_mask'])
# print('first of outputs:', outputs)
predictions = outputs.logits.argmax(dim=-1)
print('\nlogits argmax:', predictions)

# %% calculate accuracy metric using `evaluate.compute` and `pipeline`
task_evaluator = evaluate.evaluator('text-classification')
pipe = pipeline('text-classification', model=model_path)
# pipeline takes model's output and wraps it in a dictionary with human-readable labels
# evaluator.compute expects the output to be in numeric form, so provide a mapping from
# the labels generated by the pipeline to numeric values present in the dataset
# alternatively, pass the model instead of wrapping it in a pipeline
eval_results = task_evaluator.compute(
  model_or_pipeline=pipe,
  data=data,
  label_mapping={
    'LABEL_0': 0,
    'LABEL_1': 1,
    'LABEL_2': 2,
    'LABEL_3': 3,
    'LABEL_4': 4,
    'LABEL_5': 5,
  },
)
print('evaluation results:', eval_results)

# %% calculate accuracy, precision, recall & f1 score for the test dataset
accuracy = evaluate.load('accuracy')
result = accuracy.compute(predictions=predictions, references=data['label'])
print('result:', result)

metrics = [evaluate.load('precision'), evaluate.load('recall'), evaluate.load('f1')]
for metric in metrics:
  scheme = 'weighted'
  result = metric.compute(
    predictions=predictions, references=data['label'], average=scheme
  )
  print(f'\n{scheme} result:', result)

# %% TODO: why is the pipeline returning probabilities but we get numbers as model's output when using
# label mappings are created in the result returned by the pipeline wrapping our model
print('first three samples:', data[:3])
sample_texts = data['text'][:3]
pipe = pipeline('text-classification', model=model_path)
for text in sample_texts:
  result = pipe(text)
  print(f'Input Text: {text}\nPrediction: {result}\n')
