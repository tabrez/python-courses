# %% import packages
import torch
from transformers import BertForMaskedLM, BertModel, BertTokenizer, pipeline

# %% using bert model with a pipeline

unmasker = pipeline('fill-mask', model='bert-base-uncased')

unmasker("Hello I'm a [MASK] model.")

# %% extract input features using BertModel
model_name: str = 'bert-base-uncased'
tokenizer = BertTokenizer.from_pretrained(model_name)
model = BertModel.from_pretrained(model_name)
print('model:', model)

input_text = 'My favourite game of strategy is chess'
input_tokenized = tokenizer(input_text, return_tensors='pt')
print('tokenized input:', input_tokenized)

outputs = model(**input_tokenized)

print('outputs:', outputs)
print('output logits:', outputs.keys())
print('shape of last_hidden_state:', outputs.last_hidden_state[0].shape)

# %% predict masked token using BertForMaskedLM
model_name = 'bert-base-uncased'
tokenizer = BertTokenizer.from_pretrained(model_name)
model = BertForMaskedLM.from_pretrained(model_name)

# Prepare the inputs
input_text = 'My favourite [MASK] game is Chess'
tokenized_encode_ids = tokenizer.encode(input_text)
tokenized_ids = tokenizer(input_text, return_tensors='pt')
print('==================================================================')
print('token encode ids:', tokenized_encode_ids)
print('token ids:', tokenized_ids)
print('input_ids:', tokenized_ids['input_ids'])
print('len(input_ids):', len(tokenized_ids['input_ids'][0]))

# Predict all tokens
with torch.no_grad():
  outputs = model(**tokenized_ids)
  print('outputs:', outputs)
  output_attributes = [
    attr
    for attr in dir(outputs)
    if not callable(getattr(outputs, attr)) and not attr.startswith('__')
  ]
  print('output attributes:', output_attributes)

  print('outputs.logits shape:', outputs.logits.shape)
  print('len of logits[0][0]:', len(outputs.logits[0][0]))
  print('outputs.logits:', outputs.logits)
  predictions = outputs.logits
  print('predictions:', predictions)
  print('predictions shape:', predictions.shape)

# Find the masked token index
print('mask_token_id:', tokenizer.mask_token_id)
mask_token_indices = torch.where(tokenized_ids['input_ids'] == tokenizer.mask_token_id)
print('mask token indices:', mask_token_indices)

# Get the most likely token ID for the masked token
predicted_token_id = predictions[0, mask_token_indices[1]].argmax(axis=-1)
print('predicted token id:', predicted_token_id)

# Convert the token ID to a string
predicted_token = tokenizer.decode(predicted_token_id)
print('Predicted token:', predicted_token)

# %% inspect results
length = len(tokenized_ids['input_ids'][0])
print('length of input ids:', length)
p = [outputs.logits[0][i].argmax() for i in range(length)]
print('predictions:', p)
print('length of predictions:', len(p))
print('all word predictions:', tokenizer.decode(p))
print('length of predicted words:', len(tokenizer.decode(p).split()))
