# https://huggingface.co/learn/nlp-course/chapter8/4?fw=pt
# %% import packages
import evaluate
from datasets import load_dataset
from transformers import (
  AutoModelForSequenceClassification,
  AutoTokenizer,
  Trainer,
  TrainingArguments,
)

raw_datasets = load_dataset('glue', 'mnli')

model_checkpoint = 'distilbert-base-uncased'
tokenizer = AutoTokenizer.from_pretrained(model_checkpoint)


def preprocess_function(examples):
  return tokenizer(examples['premise'], examples['hypothesis'], truncation=True)


tokenized_datasets = raw_datasets.map(preprocess_function, batched=True)
model = AutoModelForSequenceClassification.from_pretrained(model_checkpoint)

args = TrainingArguments(
  output_dir='/home/tabrez/.huggingface/results/distilbert-finetuned-mnli',
  evaluation_strategy='epoch',
  save_strategy='epoch',
  learning_rate=2e-5,
  num_train_epochs=3,
  weight_decay=0.01,
)

metric = evaluate.load('glue', 'mnli')


def compute_metrics(eval_pred):
  predictions, labels = eval_pred
  return metric.compute(predictions=predictions, references=labels)


trainer = Trainer(
  model,
  args,
  train_dataset=tokenized_datasets['train'],
  eval_dataset=tokenized_datasets['validation_matched'],
  compute_metrics=compute_metrics,
)
print('dataset:', trainer.train_dataset[0])
print('dataset keys:', trainer.train_dataset[0].keys())
print(type(trainer.model))
print('label names:', trainer.train_dataset.features['label'].names)

#%%
trainer.train()

# %%
