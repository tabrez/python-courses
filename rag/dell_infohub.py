## Some mistakes in the code: https://infohub.delltechnologies.com/p/using-retrieval-augmented-generation-rag-on-a-custom-pdf-dataset-with-dell-technologies/
## https://github.com/dell-examples/generative-ai/blob/main/pytorch/RAG-llama2-custom-pdfs/RAG-llama-chatbot-custom-pdf-publish.ipynb

#%% import packages
import gc
import os

import torch
from auto_gptq import AutoGPTQForCausalLM
from langchain import HuggingFacePipeline, PromptTemplate
from langchain.chains import RetrievalQA
from langchain.document_loaders import PyPDFDirectoryLoader
from langchain.embeddings import HuggingFaceInstructEmbeddings
from langchain.text_splitter import RecursiveCharacterTextSplitter
from langchain.vectorstores import Chroma
from pdf2image import convert_from_path
from transformers import AutoTokenizer, TextStreamer, pipeline

## clean up
device = 'cuda:0' if torch.cuda.is_available() else 'cpu'
gc.collect()
torch.cuda.empty_cache()
# rm -rf 'db'

os.getcwd()
# !wget https://www.delltechnologies.com/asset/en-us/solutions/apex/briefs-summaries/apex-navigator-for-multicloud-storage-solution-overview.pdf
## preview a pdf file
pdf_images = convert_from_path('pdfs_dell_infohub/apex-navigator-for-multicloud-storage-solution-overview.pdf')
pdf_images[0]

#%% how many pages we are dealing with
loader = PyPDFDirectoryLoader('pdfs_dell_infohub')
docs = loader.load()
print('type of docs:', type(docs), 'len of docs:', len(docs), 'type of docs[0]:', type(docs[0]))
print('docs[0]:', docs[1])

#%% split the pages into chunks of data
embeddings = HuggingFaceInstructEmbeddings(
  model_name='hkunlp/instructor-large', model_kwargs={'device': device}
)

text_splitter = RecursiveCharacterTextSplitter(chunk_size=1024, chunk_overlap=64)
texts = text_splitter.split_documents(docs)
print('len of texts:', len(texts), 'type(texts):', type(texts), 'type(texts[0]):', texts[0])

chromadb = Chroma.from_documents(texts, embeddings, persist_directory='db')

#%% prepare model
model_name = 'TheBloke/Llama-2-13B-chat-GPTQ'
model_basename = 'model'
tokenizer = AutoTokenizer.from_pretrained(model_name, use_fast=True)

model = AutoGPTQForCausalLM.from_quantized(
  model_name,
  revision='gptq-4bit-128g-actorder_True',
  model_basename=model_basename,
  use_safetensors=True,
  trust_remote_code=True,
  inject_fused_attention=False,
  device=device,
  quantize_config=None
)

streamer = TextStreamer(tokenizer, skip_prompt=True, skip_special_tokens=True)

#%% create pipeline
text_pipeline = pipeline(
  'text-generation',
  model=model,
  tokenizer=tokenizer,
  max_new_tokens=1024,
  # temperature=0,
  do_sample=True,
  top_p=0.95,
  repetition_penalty=1.15,
  streamer=streamer
)

llm = HuggingFacePipeline(pipeline=text_pipeline, model_kwargs={'temperature': 0})

#%% prepare prompt
DEFAULT_SYSTEM_PROMPT = """
You are a helpful, respectful and honest assistant. Always answer as helpfully as possible, while being safe. Your answers should not include any harmful, unethical, racist, sexist, toxic, dangerous, or illegal content. Please ensure that your responses are socially unbiased and positive in nature.

If a question does not make any sense, or is not factually coherent, explain why instead of answering something incorrectly. If you don't know the answer to a question, please don't share false information.
""".strip()

def generate_prompt(prompt: str, system_prompt: str = DEFAULT_SYSTEM_PROMPT) -> str:
  return f"""
[INST] <<SYS>>
{system_prompt}
<</SYS>>

{prompt} [/INST]
""".strip()

SYSTEM_PROMPT = "Use the following pieces of context to answer the question at the end. If you don't know the answer, just say that you don't know, don't try to make up an answer."

template = generate_prompt(
    """
{context}

Question: {question}
""", system_prompt=SYSTEM_PROMPT,
)

prompt = PromptTemplate(template=template, input_variables=['context', 'question'])

# %%
qa_chain = RetrievalQA.from_chain_type(
  llm=llm,
  chain_type='stuff',
  retriever=chromadb.as_retriever(search_kwargs={'k': 2}),
  return_source_documents=True,
  chain_type_kwargs={'prompt': prompt}
)

#%%
import time

start = time.time()
# query = 'provide a curl example of code to authenticate my powerflex'
query = 'Does apex file storage support multi availability zones?'
results = qa_chain(query)
print('execution time:', time.time() - start, ' seconds.')

# %%

# query = 'provide a curl example of code to authenticate my powerflex'
# query = 'Does apex file storage support multi availability zones?'
# query = 'What does cnvrg.io platform enable in regards to ML deployment and management? Explain about Orchestrator and workload scheduler.'
query = 'Which of the following can be used as an orchestration, scheduling, and scaling layer for cnvrg.io platform: VMware Tanzu, RedHat OpenShift, scala, SUSE Rancher, ahsan, tabrez'
print('=========================================================')
# vectordb.similarity_search_with_score(query)
