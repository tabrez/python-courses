# %% Pipeline
from transformers import pipeline

classifier = pipeline('sentiment-analysis', model='distilbert-base-uncased-finetuned-sst-2-english')
result = classifier('We are very happy to show you the transformers library.')
print(f'result: {result}')
results = classifier(["We are very happy to show you the Transformers library.", "We hope you don't hate it."])
for result in results:
  print(f"label: {result['label']}, with score: {round(result['score'])}")

# %% Audio dataset
from transformers import pipeline

from datasets import load_dataset, Audio
speech_recognizer = pipeline("automatic-speech-recognition", model="facebook/wav2vec2-base-960h")
dataset = load_dataset("PolyAI/minds14", name="en-US", split="train")
dataset = dataset.cast_column("audio", Audio(sampling_rate=speech_recognizer.feature_extractor.sampling_rate))
results = speech_recognizer(dataset[:4]["audio"])
print([d["text"] for d in results])


# %% Use a different model & tokenizer
# Get the model name from the hub: https://huggingface.co/models
from transformers import AutoTokenizer, AutoModelForSequenceClassification
model_name = "nlptown/bert-base-multilingual-uncased-sentiment"
model = AutoModelForSequenceClassification.from_pretrained(model_name)
tokenizer = AutoTokenizer.from_pretrained(model_name)
classifier = pipeline("sentiment-analysis", model=model, tokenizer=tokenizer)
classifier("Nous sommes très heureux de vous présenter la bibliothèque 🤗 Transformers.")

# %% AutoTokenizer
from transformers import AutoTokenizer
model_name = "nlptown/bert-base-multilingual-uncased-sentiment"
tokenizer = AutoTokenizer.from_pretrained(model_name)
encoding = tokenizer("We are very happy to show you the Transformers library.")
print(encoding)

pt_batch = tokenizer(
  ["We are very happy to show you the Transformers library.",
   "We hope you don't hate it."],
  padding=True,
  truncation=True,
  max_length=512,
  return_tensors="pt"
)
print(pt_batch)

# %% AutoModel
from transformers import AutoModelForSequenceClassification
from torch import nn

model_name="nlptown/bert-base-multilingual-uncased-sentiment"
pt_model=AutoModelForSequenceClassification.from_pretrained(model_name)
pt_outputs=pt_model(**pt_batch)
print(pt_outputs)

pt_predictions = nn.functional.softmax(pt_outputs.logits, dim=1)
print(pt_predictions)

# %% saving and reloading fine-tuned model with its tokenizer
pt_save_directory="./pt_save_pretrained"
tokenizer.save_pretrained(pt_save_directory)
pt_model.save_pretrained(pt_save_directory)

pt_model = AutoModelForSequenceClassification.from_pretrained("./pt_save_pretrained")

# %% Trainer - a PyTorch optimized training loop
from transformers import AutoModelForSequenceClassification
from transformers import TrainingArguments
from transformers import AutoTokenizer
from datasets import load_dataset
from transformers import DataCollatorWithPadding
from transformers import Trainer

## Create model from a PreTrainedModel or a torch.nn.Module
model = AutoModelForSequenceClassification.from_pretrained("distilbert-base-uncased")

## TrainingArguments contain the model hyperparameters
training_args = TrainingArguments(
  output_dir="./save_trained",
  learning_rate=2e-5,
  per_device_train_batch_size=8,
  per_device_eval_batch_size=8,
  num_train_epochs=2
)

## Preprocessing class like a tokenizer, image processor, feature extractor, etc.
tokenizer = AutoTokenizer.from_pretrained("distilbert-base-uncased")

## Load a dataset
dataset = load_dataset("rotten_tomatoes")
def tokenize_dataset(dataset):
  return tokenizer(dataset["text"])
dataset = dataset.map(tokenize_dataset, batched=True)

## Create a batch of examples from your dataset
data_collator=DataCollatorWithPadding(tokenizer=tokenizer)

## Gather all these objects in Trainer and train the model
trainer = Trainer(
  model=model,
  args=training_args,
  train_dataset=dataset["train"],
  eval_dataset=dataset["test"],
  tokenizer=tokenizer,
  data_collator=data_collator
)

trainer.train()

# %% make predictions
trainer.predict(dataset["test"])

# %%
# 6. save and load model
tokenizer.save_pretrained("./first_ex/")
trainer.save_model("./first_ex/")

# tokenizer = AutoTokenizer.from_pretrained("./first_ex/")
# model = AutoModelForSequenceClassification.from_pretrained("./first_ex/")
