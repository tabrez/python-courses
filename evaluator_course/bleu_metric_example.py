# %% import packages
from datasets import load_dataset, load_metric
from tqdm import tqdm
from transformers import MarianMTModel, MarianTokenizer

# Load the dataset for German to English, the model and the tokenizer for German to English
# https://huggingface.co/datasets/wmt14/viewer/de-en/test
dataset = load_dataset('wmt14', 'de-en', split='test[:1%]')
# https://huggingface.co/Helsinki-NLP/opus-mt-de-en
model_name = 'Helsinki-NLP/opus-mt-de-en'
tokenizer = MarianTokenizer.from_pretrained(model_name)
model = MarianMTModel.from_pretrained(model_name)

# %% inspect a sample in the dataset
for example in dataset:
  print('type of example:', type(example))
  print('len of example:', len(example))
  print('fields of example:', example.items())
  print('inner dict of example:', example['translation'])
  print('de field of example:', example['translation']['de'])
  print('en field of example:', example['translation']['en'])
  break


# Function to translate text from German to English
def translate(text):
  inputs = tokenizer(
    text, return_tensors='pt', padding=True, truncation=True, max_length=512
  )
  outputs = model.generate(**inputs)
  return tokenizer.decode(outputs[0], skip_special_tokens=True)


# %%
bleu = load_metric('bleu')

# predictions = [translate(example['translation']['de'].split(' ')) for example in dataset]
predictions = [
  tokenizer.tokenize(translate(example['translation']['de']))
  for example in tqdm(dataset, desc='translating')
]
print('type of predictions:', type(predictions))
print('len of predictions:', len(predictions))
print('first element of predictions:', predictions[0])

# references = [[example['translation']['en'].split(' ')] for example in dataset]
references = [
  [tokenizer.tokenize(example['translation']['en'])]
  for example in tqdm(dataset, desc='translating')
]
print('type of predictions:', type(predictions))
print('len of predictions:', len(predictions))
print('first element of predictions:', predictions[0])

# %%
# Calculate BLEU score using the evaluate library
# gives error: ValueError: Got a string but expected a list instead: 'Gutach:'
bleu_score = bleu.compute(predictions=[predictions], references=[references])

print('BLEU score:', bleu_score['bleu'])

# %% Code that fixes the above error. Above code is retained for now for the print
# statements.

# Load and shuffle the dataset for German to English and select 3 random samples
dataset = load_dataset('wmt14', 'de-en', split='test')
print('test split of wmt14 dataset:', dataset)
dataset = dataset.shuffle(seed=42).select(range(3000))

# Load the model and tokenizer for German to English
model_name = 'Helsinki-NLP/opus-mt-de-en'
tokenizer = MarianTokenizer.from_pretrained(model_name)
model = MarianMTModel.from_pretrained(model_name)


# Function to translate text from German to English
def translate(text):
  inputs = tokenizer(
    text, return_tensors='pt', padding=True, truncation=True, max_length=512
  )
  outputs = model.generate(**inputs)
  return tokenizer.decode(outputs[0], skip_special_tokens=True)


bleu = load_metric('bleu')

# Prepare the data for BLEU calculation
translations = []
references = []

for example in tqdm(dataset, desc='Translating and tokenizing'):
  # Translate and tokenize the German text
  german_translation = translate(example['translation']['de'])
  translations.append(tokenizer.tokenize(german_translation))

  # Tokenize the English reference
  english_reference = example['translation']['en']
  references.append([tokenizer.tokenize(english_reference)])

# Calculate BLEU score
bleu_score = bleu.compute(predictions=translations, references=references)

print('BLEU score:', bleu_score['bleu'])

# %%
chrf = load_metric('chrf')
chrf_score = chrf.compute(predictions=translations, references=references)
print('chrF score:', chrf_score)
