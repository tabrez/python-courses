#%% import packages
import pprint

from datasets import load_dataset

#%% download files and check if they're accessible
# !wget https://github.com/crux82/squad-it/raw/master/SQuAD_it-train.json.gz
# !wget https://github.com/crux82/squad-it/raw/master/SQuAD_it-test.json.gz
# !gzip -dkv SQuAD_it-*.json.gz

filename = './SQuAD_it-train.json'
squad_it_ds = load_dataset('json', data_files=filename, field='data')
print('squad_it_ds', squad_it_ds)

pp = pprint.PrettyPrinter(indent=2).pprint
pp('title of first sample:')
pp(squad_it_ds['train'][0]['title'])

#%% create dataset from local files
data_files = {'train': 'SQuAD_it-train.json', 'test': 'SQuAD_it-test.json'}
## also works:
# data_files = {'train': 'SQuAD_it-train.json.gz', 'test': 'SQuAD_it-test.jzon.gz'}
squad_it_ds = load_dataset('json', data_files=data_files, field='data')
print('squad_it_ds', squad_it_ds)

#%% create dataset from remote files
url = "https://github.com/crux82/squad-it/raw/master/"
data_files = {
  'train': url + 'SQuAD_it-train.json.gz',
  'test': url + 'SQuAD_it-test.json.gz'
  }
squad_it_ds = load_dataset('json', data_files=data_files, field='data')
print('squad_it_ds', squad_it_ds)

#%% Slicing and dicing our data
# !wget "https://archive.ics.uci.edu/ml/machine-learning-databases/00462/drugsCom_raw.zip"
# !unzip drugsCom_raw.zip

data_files = { 'train': 'drugsComTrain_raw.tsv', 'test': 'drugsComTest_raw.tsv' }
drug_ds = load_dataset('csv', data_files=data_files, delimiter='\t')
print('drug_ds', drug_ds)

## select a small sample of the dataset
drug_ds_sample = drug_ds['train'].shuffle(seed=42).select(range(1000))
pp('first 3 samples:')
pp(drug_ds_sample[:3])
# len(drug_ds['train'].unique('Unnamed: 0')), len(drug_ds['train'])
# len(drug_ds['train'].unique('drugName')), len(drug_ds['train'].unique('condition'))

#%% normalize all the condition labels
def filter_nones_condition(x):
  return x['condition'] is not None

def lowercase_condition(example):
  return { 'condition': example['condition'].lower() }

drug_ds = drug_ds.filter(filter_nones_condition).map(lowercase_condition)
pp('first 3 samples:')
pp(drug_ds['train'][:3])

#%% creating new columns
def compute_review_length(example):
  return {'review_length': len(example['review'].split())}
drug_ds = drug_ds.map(compute_review_length)

#%% print first three samples, three samples with shortest and longest reviews each
pp(drug_ds['train'][:3])
pp(drug_ds['train'].sort('review_length')[:3])
## exercise: Use the Dataset.sort() function to inspect the reviews with the largest numbers of words.
pp(drug_ds['train'].sort('review_length')[-3:])

drug_ds = drug_ds.filter(lambda x: x['review_length'] > 30)
print('no. of rows:', drug_ds['train'].num_rows)

#%% fix html escape characters
import html

text = 'I&#039;m a transformer called BERT'
print('text before:', text)
text = html.unescape(text)
print('text after:', text)

drug_ds = drug_ds.map(lambda x: {'review': html.unescape(x['review'])})
print('first 3 reviews:', drug_ds['train']['review'][:3])

#%%time batched map
drug_ds = drug_ds.map(
  lambda batch: {'review': [html.unescape(x) for x in batch['review']]},
  batched=True
)

#%% use tokenizer to check time difference when using batched operations
## experiment with `use_Fast=True|False`, `batched=True|False`, `num_proc=1|8`
from transformers import AutoTokenizer

tokenizer = AutoTokenizer.from_pretrained('bert-base-cased')

#%%
def tokenize_sample(samples):
  return tokenizer(samples['review'], truncation=True)
drug_ds = drug_ds.map(tokenize_sample, batched=True)

#%% split 'review' column into multiple columns with each containing 128 chars maximum
def tokenize_and_split(examples, _has_not_printed=[True]):
  # for i in range(len(examples['review'])):
  #   e = {key: value[i] for key, value in examples.items()}
  #   print(f'example {i}:')
  #   pp(e)
  if _has_not_printed[0]:
    print('examples:', type(examples))
    print('examples:', len(examples['review']))
    _has_not_printed[0] = False

  return tokenizer(
    examples['review'],
    truncation=True,
    max_length=128,
    return_overflowing_tokens=True
  )
# print('[0]:', drug_ds['train'][1])
# result = tokenize_and_split(drug_ds['train'][1])
# print('result:', result)
# for inp in result['input_ids']:
#   print('inp:', inp)
# print('len of each column:', [len(c) for c in result['input_ids']])
print('number of rows:', len(drug_ds['train']))

## tokenize all samples
# drug_ds_2500 = drug_ds['train'].select(range(200))
tokenized_ds = drug_ds.map(tokenize_and_split,
                     remove_columns=drug_ds['train'].column_names,
                     batched=True,
                     batch_size=1000)
print('len train ds:', len(drug_ds['train']))
print('len tokenized train ds:', len(tokenized_ds['train']))
print('len tokenized test:', len(tokenized_ds['test']))

#%% convert huggingface dataset to pandas dataframe
drug_ds.set_format('pandas')
drug_df = drug_ds['train'][:]
print('drug_df:', drug_df[:3])

# from datasets import Dataset
# drug_ds = Dataset.from_pandas(drug_df)

drug_ds.reset_format()

#%% create a validation set
## drug_ds['test'] is a test set, keep it for testing and create a new split for validation dataset
drug_ds_clean = drug_ds['train'].train_test_split(train_size=0.8, seed=42)
drug_ds_clean['validation'] = drug_ds_clean.pop('test')
drug_ds_clean['test'] = drug_ds['test']
print('drug_ds_clean:', drug_ds_clean)

#%% save and load the dataset
drug_ds_clean.save_to_disk('drug-reviews')

from datasets import load_from_disk

drug_ds_loaded = load_from_disk('drug-reviews')
print('drug_ds_loaded:', drug_ds_loaded)

## save and load in json format
for split, dataset in drug_ds_clean.items():
  dataset.to_json(f'drug-reviews-{split}.jsonl')
# !head -n 2 drug-reviews-train.jsonl
# {"patient_id":141780,"drugName":"Escitalopram","condition":"depression","review":"\"I seemed to experience the regular side effects of LEXAPRO, insomnia, low sex drive, sleepiness during the day. I am taking it at night because my doctor said if it made me tired to take it at night. I assumed it would and started out taking it at night. Strange dreams, some pleasant. I was diagnosed with fibromyalgia. Seems to be helping with the pain. Have had anxiety and depression in my family, and have tried quite a few other medications that haven't worked. Only have been on it for two weeks but feel more positive in my mind, want to accomplish more in my life. Hopefully the side effects will dwindle away, worth it to stick with it from hearing others responses. Great medication.\"","rating":9.0,"date":"May 29, 2011","usefulCount":10,"review_length":125}

data_files = {
  'train': 'drug-reviews-train.jsonl',
  'validation': 'drug-reviews-validation.jsonl',
  'test': 'drug-reviews-test.jsonl'
}
drug_ds_reloaded = load_dataset('json', data_files=data_files)

#%% exercise: Use the techniques from Chapter 3 to train a classifier that can predict the patient condition based on the drug review.

#%% exercise: Use the summarization pipeline from Chapter 1 to generate summaries of the reviews.

# %% check if the current working directory is correct for the python kernel
import json

# import os

# os.chdir('part2')
# print(os.getcwd())

filename = './SQuAD_it-train.json'
with open(filename, 'r') as f:
  print(json.load(f))
  print(json.load(f))
