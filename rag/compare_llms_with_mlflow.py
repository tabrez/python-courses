## https://github.com/djliden/llmops-examples/blob/main/compare-llms-with-mlflow.ipynb

#!pip install transformers accelerate torch mlflow xformers

#%%
import mlflow
import pandas as pd
from transformers import (AutoModelForCausalLM, AutoTokenizer,
                          GenerationConfig, pipeline)


## Define the models
class PyfuncTransformer(mlflow.pyfunc.PythonModel):
  def __init__(self, model_name, gen_config_dict=None, examples=''):
    self.model_name = model_name
    self.gen_config_dict = (
      gen_config_dict if gen_config_dict is not None else {}
    )
    self.examples = examples
    super().__init__()

def load_context(self, context):
  """
  Loads the model and tokenizer using the specified model_name.

  Args:
    context: The MLflow context
  """
  tokenizer = AutoTokenizer.from_pretrained(self.model_name)
  model = AutoModelForCausalLM.from_pretrained(
    self.model_name,
    device_map='auto'
  )

  ## Create a custom GenerationConfig
  gcfg = GenerationConfig.from_model_config(model.config)
  for key, value in self.gen_config_dict.items():
    if hasattr(gcfg, key):
      setattr(gcfg, key, value)

  ## Apply the GenerationConfig to the model's config
  model.config.update(gcfg.to_dict())

  self.model = pipeline(
    'text-generation',
    model=model,
    tokenizer=tokenizer,
    return_full_text=False
  )

def predict(self, context, model_input):
  """
  Generates text based on the provider model_input using the loaded model.

  Args:
    context:  The MLflow context.
    model_input: The input used for generating the text.

  Returns:
    list: A list of generated texts.
  """
  if isinstance(model_input, pd.DataFrame):
    model_input = model_input.values.flatten().tolist()
  elif not isinstance(model_input, list):
    model_input = [model_input]

  generated_text = []
  for input_text in model_input:
    output = self.model(
      self.examples + input_text, return_full_text=False
    )
    generated_text.append(
      output[0]['generated_text']
    )

  return generated_text

## Instantiate the models
gcfg = {
  'max_length': 180,
  'max_new_tokens': 10,
  'do_sample': False,
}

examples = (
    "Q: Are elephants larger than mice?\nA: Yes.\n\n"
    "Q: Are mice carnivorous?\nA: No, mice are typically omnivores.\n\n"
    "Q: What is the average lifespan of an elephant?\nA: The average lifespan of an elephant in the wild is about 60 to 70 years.\n\n"
    "Q: Is Mount Everest the highest mountain in the world?\nA: Yes.\n\n"
    "Q: Which city is known as the 'City of Love'?\nA: Paris is often referred to as the 'City of Love'.\n\n"
    "Q: What is the capital of Australia?\nA: The capital of Australia is Canberra.\n\n"
    "Q: Who wrote the novel '1984'?\nA: The novel '1984' was written by George Orwell.\n\n"
)

bloom560 = PyfuncTransformer(
  'bigscience/bloom-560m',
  gen_config_dict=gcfg,
  examples=examples,
)

gpt2large = PyfuncTransformer(
  'gpt2-large',
  gen_config_dict=gcfg,
  examples=examples,
)

distilgpt2 = PyfuncTransformer(
  'distilgpt2',
  gen_config_dict=gcfg,
  examples=examples,
)

#%%
## Log the Models with MLFlow
mlflow.set_experiment(experiment_name='complare_small_models')
run_ids = []
artifact_paths = []
model_names = ['bloom-560m', 'gpt2-large', 'distilgpt2']

for model, name in zip([bloom560, gpt2large, distilgpt2], model_names):
  with mlflow.start_run(run_name=f'log_model_{name}'):
    pyfunc_model = model
    artifact_path = f'models/{name}'
    mlflow.pyfunc.log_model(
      artifact_path=artifact_path,
      python_model=pyfunc_model,
      input_example=['Q: What color is the sky?\nA:'],
      signature=False
    )
    run_ids.append(mlflow.active_run().info.run_id)
    artifact_paths.append(artifact_path)

#%%
## Set up the evaluation data

eval_df = pd.DataFrame(
  {
    'question': [
      ['Q: What color is the sky?\nA:'],
      ['Q: Are trees plants or animals?\nA:'],
      ['Q: What is 2+2?\nA:'],
      ['Q: Who is Darth Vader?\nA:'],
      ['Q: What is your favourite color?\nA:'],
    ]
  }
)
print(eval_df)

#%%
## Compare the models with mlflow.evaluate()

for i in range(3):
  with mlflow.start_run(
    run_id=run_ids[i]
  ):
    evaluation_results = mlflow.evaluate(
      model=f'runs:/{run_ids[i]}/{artifact_paths[i]}',
      model_type='text',
      data=eval_df,
    )

## Load the results table

mlflow.load_table('eval_results_table.json')

### Part 2: Comparing generation parameters at inference time

## We can modify the approach above to accept generation configuration parameters at inference time,
## so we can compare many of the same inputs with different generation configurations and track
## those configurations in the evaluation table.


# %%
