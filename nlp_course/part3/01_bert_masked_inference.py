#%% import packages
from datasets import load_dataset
from transformers import AutoTokenizer, AutoModelForMaskedLM

#%% load the model & tokenizer
bert_checkpoint = 'bert-base-uncased'
# bert_checkpoint = 'roberta-base'

tokenizer = AutoTokenizer.from_pretrained(bert_checkpoint)
text = 'I want to skip breakfast [MASK] morning as I need to get to work'
tokenized = tokenizer(text, return_tensors='pt')
print('tokenized:', tokenized)

#%%
model = AutoModelForMaskedLM.from_pretrained(bert_checkpoint)
# print(model)
outputs = model(**tokenized)
# print('outputs:', outputs)

predicted_ids = outputs.logits[0].argmax(-1)
# print('predicted token ids:', predicted_ids)
predicted_text = tokenizer.decode(predicted_ids)
print('predicted text:', predicted_text)
