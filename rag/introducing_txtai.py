## https://github.com/neuml/txtai/blob/master/examples/01_Introducing_txtai.ipynb

#%% import packages
import time
from txtai import Embeddings

data = [
  "US tops 5 million confirmed virus cases",
  "Canada's last fully intact ice shelf has suddenly collapsed, forming a Manhattan-sized iceberg",
  "Beijing mobilises invasion craft along coast as Taiwan tensions escalate",
  "The National Park Service warns against sacrificing slower friends in a bear attack",
  "Maine man wins $1M from $25 lottery ticket",
  "Make huge profits without work, earn up to $100,000 a day"
]

embeddings = Embeddings(path='sentence-transformers/nli-mpnet-base-v2')
embeddings.index(data)

print('%-20s %s' % ('Query', 'Best Match'))
# print(f'{"Query":<20} {"Best Match"}')
print('-' * 50)

for query in ('feel good story', 'climate change', 'public health story', 'war', 'wildlife', 'asia', 'lucky', "dishonest junk"):
  uid = embeddings.search(query, 1)[0][0]
  print('%-20s %s' % (query, data[uid]))

# %%
# embeddings.save('db/index')

#%%
embeddings.load('db/index')

for query in ('feel good story', 'climate change', 'public health story', 'war', 'wildlife', 'asia', 'lucky', "dishonest junk"):
  uid = embeddings.search(query, 1)[0][0]
  print('%-20s %s' % (query, data[uid]))
