#%% import packages
from datasets import load_dataset
from torch.utils.data import DataLoader
from tqdm.auto import tqdm
from transformers import (AdamW, AutoModelForSequenceClassification,
                          AutoTokenizer, DataCollatorWithPadding,
                          get_scheduler)

#%% create dataloaders

raw_ds = load_dataset('glue', 'mrpc')
checkpoint = 'bert-base-uncased'
tokenizer = AutoTokenizer.from_pretrained(checkpoint)

def tokenize_batch(batch):
  return tokenizer(batch['sentence1'], batch['sentence2'], truncation=True)

ds = raw_ds.map(tokenize_batch, batched=True)
ds = ds.remove_columns(['sentence1', 'sentence2', 'idx'])
ds = ds.rename_column('label', 'labels')
ds.set_format('torch')
data_collator = DataCollatorWithPadding(tokenizer=tokenizer)
bs = 128
train_dl = DataLoader(dataset=ds['train'],
                      shuffle=True,
                      batch_size=bs,
                      collate_fn=data_collator)
valid_dl = DataLoader(dataset=ds['validation'], batch_size=bs, collate_fn=data_collator)

#%% create model
model = AutoModelForSequenceClassification.from_pretrained(checkpoint, num_labels=2)
model.to('cuda')
optimizer = AdamW(params=model.parameters(), lr=1e-5, no_deprecation_warning=True)
epochs = 3
num_training_steps = epochs * len(train_dl)
lr_scheduler = get_scheduler('linear',
                          optimizer=optimizer,
                          num_warmup_steps=0,
                          num_training_steps=num_training_steps)
progress_bar = tqdm(range(num_training_steps))

#%% training loop
loss = 0
for epoch in range(epochs):
  for batch in train_dl:
    batch = {k: v.to('cuda') for k, v in batch.items()}
    outputs = model(**batch)
    loss = outputs.loss
    loss.backward()

    optimizer.step()
    lr_scheduler.step()
    optimizer.zero_grad()
    print('epoch: {epoch}, loss: {loss}')
    progress_bar.update(1)

print('loss: ', loss)
