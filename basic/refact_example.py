from transformers import AutoModelForCausalLM, AutoTokenizer
import torch

model_name = 'smallcloudai/Refact-1_6B-fim'
device = "cuda"

tokenizer = AutoTokenizer.from_pretrained(model_name)
model = AutoModelForCausalLM.from_pretrained(model_name, trust_remote_code=True).to(device).half()
prompt = '<fim_prefix>def print_hello_world():\n    """<fim_suffix>\n    print("Hello world!")<fim_middle>'

input = tokenizer.encode(prompt, return_tensors='pt').to(device).to(torch.float16)

output = model.generate(input, max_length=100, temperature=0.2)
print(f'output: {tokenizer.decode(output[0])}')
