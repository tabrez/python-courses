#%% import packages
import collections
import xml.etree.ElementTree as ET

from langchain.embeddings import HuggingFaceBgeEmbeddings
from langchain.schema import Document
from langchain.text_splitter import CharacterTextSplitter
from langchain.vectorstores import Chroma

## Load XML file
#!wget https://github.com/dssjon/biblos/raw/main/data/engwebp_vpl.xml
tree = ET.parse('./data/engwebp_vpl.xml')
root = tree.getroot()

verses_by_chapter = collections.defaultdict(list)
for verse in root.findall('v'):
  book = verse.attrib['b']
  chapter = int(verse.attrib['c'])
  verse_num = int(verse.attrib['v'])
  text = verse.text

  verses_by_chapter[(book, chapter)].append((verse_num, text))

documents = []
for (book, chapter), verses in verses_by_chapter.items():
  chapter_text = ''
  for verse_num, text in verses:
    chapter_text += f'{text}\n'

  verse_nums_as_string = ','.join(str(verse_num) for verse_num, _ in verses)
  doc = Document(page_content=chapter_text)
  doc.metadata = {
    'book': book,
    'chapter': chapter,
    'verse_nums': verse_nums_as_string
  }
  print('doc:', doc)

  documents.append(doc)

#%% split each document into chunks
verse_splitter = CharacterTextSplitter(
  separator='\n',
  chunk_size=1000,
  chunk_overlap=100
)
bible = verse_splitter.split_documents(documents)

#%% load embeddings
model_name = 'BAAI/bge-large-en-v1.5'
model_kwargs = {'device': 'cuda:0'}
encode_kwargs = {'normalize_embeddings': True}

embedding_function = HuggingFaceBgeEmbeddings(
  model_name=model_name,
  model_kwargs=model_kwargs,
  encode_kwargs=encode_kwargs
)

#%% create Chroma database
db = Chroma.from_documents(
  bible,
  embedding_function,
  persist_directory='./db',
  collection_metadata={'hnsw:space': 'cosine'}
)
db.persist()

#%%
# db = Chroma(
#   persist_directory='./db',
#   embedding_function=embedding_function
# )

query = 'What did Jesus say about eternal life?'
results = db.similarity_search_with_relevance_scores(
  query,
  k=4,
  score_function='cosine'
)
print('results:', results)

# %%
