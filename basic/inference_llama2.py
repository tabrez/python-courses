#%%
# from datasets import load_dataset

# ds = load_dataset('yelp_review_full')
# print(f'ds: {ds}')
# print(f'len(ds): {len(ds)}, ds.shape: {ds.shape}')

#%%
import transformers, torch
from transformers import AutoTokenizer

model_name = "meta-llama/Llama-2-7b-chat-hf"

tokenizer = AutoTokenizer.from_pretrained(model_name)
pipeline = transformers.pipeline('text-generation',
                                 model=model_name,
                                 torch_dtype=torch.float16,
                                 device_map='auto')

options = {
  'do_sample': True,
  'top_k': 10,
  'num_return_sequences': 1,
  'eos_token_id': tokenizer.eos_token_id,
  'max_length': 1000
}
# outputs = pipeline('I loved watching "Breaking Bad", "The Wire", "Seinfeld", "30 Rock", can you recommend other shows I might like?\n', **options)
# print(f'output: {outputs[0]["generated_text"]}')

user_text=''
while True:
  user_text = input('> ')
  print(f'input is: {user_text} done')
  if (user_text == 'exit'):
    break
  outputs = pipeline(user_text + ' \n', **options)

  # print(f'len(outputs): {len(outputs)}')
  for output in outputs:
    # print(f'len(output["generated_text"]): {len(output["generated_text"])}')
    print(f'output: {output["generated_text"]}')
