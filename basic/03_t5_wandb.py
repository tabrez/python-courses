# https://wandb.ai/mukilan/T5_transformer/reports/Exploring-Google-s-T5-Text-To-Text-Transformer-Model--VmlldzoyNjkzOTE2

# %% imports

from transformers import T5ForConditionalGeneration, T5Tokenizer

# %%

tokenizer = T5Tokenizer.from_pretrained('t5-small')
model = T5ForConditionalGeneration.from_pretrained('t5-small', return_dict=True)

# %% text summarization

one_piece_sequence = (
  'The series focuses on Monkey D. Luffy, a young man made of rubber, who, inspired by his childhood idol,'
  'the powerful pirate Red-Haired Shanks, sets off on a journey from the East Blue Sea to find the mythical treasure,'
  'the One Piece, and proclaim himself the King of the Pirates. In an effort to organize his own crew, the Straw Hat Pirates,'
  'Luffy rescues and befriends a pirate hunter and swordsman named Roronoa Zoro, and they head off in search of the '
  'titular treasure. They are joined in their journey by Nami, a money-obsessed thief and navigator; Usopp, a sniper '
  'and compulsive liar; and Sanji, a perverted but chivalrous cook. They acquire a ship, the Going Merry, and engage in confrontations'
  'with notorious pirates of the East Blue. As Luffy and his crew set out on their adventures, others join the crew later in the series, '
  'including Tony Tony Chopper, an anthropomorphized reindeer doctor; Nico Robin, an archaeologist and former Baroque Works assassin; '
  'Franky, a cyborg shipwright; Brook, a skeleton musician and swordsman; and Jimbei, a fish-man helmsman and former member of the Seven '
  'Warlords of the Sea. Once the Going Merry is damaged beyond repair, Franky builds the Straw Hat Pirates a new ship, the Thousand Sunny,'
  'Together, they encounter other pirates, bounty hunters, criminal organizations, revolutionaries, secret agents, and soldiers of the'
  'corrupt World Government, and various other friends and foes, as they sail the seas in pursuit of their dreams.'
)

inputs = tokenizer.encode(
  'summarise: ' + one_piece_sequence,
  return_tensors='pt',
  max_length=512,
  truncation=True,
)
summarization_ids = model.generate(
  inputs,
  max_length=80,
  min_length=40,
  length_penalty=5.0,
  num_beams=2,
)
summarization = tokenizer.decode(summarization_ids[0])
print(summarization)

# %% language translation

language_sequence = 'You should definitely watch "One Piece", it is a good show, you \
                     will love the adventures in it'
input_ids = tokenizer(
  'translate English to French: ' + language_sequence, return_tensors='pt'
).input_ids
language_ids = model.generate(input_ids, max_length=80, min_length=30)
language_translation = tokenizer.decode(language_ids[0], skip_special_tokens=True)
print(language_translation)

# %% text classification: textual entailment


def textual_entailment(premise, hypothesis):
  input_ids = tokenizer(
    'mnli premise: ' + premise + ' hypothesis: ' + hypothesis,
    return_tensors='pt',
  ).input_ids
  entailment_ids = model.generate(input_ids, max_length=80)
  return tokenizer.decode(entailment_ids[0], skip_special_tokens=True)


premise = 'I love One Piece'
hypothesis = 'Quick fox jumped over a lazy dog'
print(textual_entailment(premise, hypothesis))

premise = 'I love One Piece'
hypothesis = 'My feelings towards One Piece is filled with love'
print(textual_entailment(premise, hypothesis))

# %% linguistic acceptability


def linguistic_acceptability(sentence):
  input_ids = tokenizer('cola: ' + sentence, return_tensors='pt').input_ids
  sentence_ids = model.generate(input_ids, max_length=80)
  return tokenizer.decode(sentence_ids[0], skip_special_tokens=True)


# sentence = ('I have tomorrow often work but why images')
sentence = 'I go blegh plagietcal'
print(linguistic_acceptability(sentence))

sentence = 'Luffy is a great pirate'
print(linguistic_acceptability(sentence))


# %% sentence similarity
def sentence_similarity(s1, s2):
  input_ids = tokenizer(
    'stsb sentence 1: ' + s1 + ' sentence 2:' + s2,
    return_tensors='pt',
  ).input_ids
  stsb_ids = model.generate(input_ids)
  return tokenizer.decode(stsb_ids[0], skip_special_tokens=True)


stsb_sentence_1 = 'Luffy was fighting in the war'
stsb_sentence_2 = 'Quick fox jumped over a lazy dog'
print(sentence_similarity(stsb_sentence_1, stsb_sentence_2))

stsb_sentence_1 = 'Luffy was fighting in the war'
stsb_sentence_2 = "Luffy's fighting style is comical"
print(sentence_similarity(stsb_sentence_1, stsb_sentence_2))
