# https://huggingface.co/docs/evaluate/a_quick_tour

# %% import packages
import evaluate
import pandas as pd
from datasets import load_dataset
from evaluate import EvaluationSuite, evaluator
from evaluate.evaluation_suite import SubTask
from evaluate.visualization import radar_plot
from transformers import pipeline

print(evaluate.load('exact_match').compute(references=['hello'], predictions=['hello']))

# %% list available modules and display their features & description
modules = evaluate.list_evaluation_modules(
  module_type='comparison', include_community=False, with_details=True
)
print('comparison modules supported by `evaluate`:', modules)

# accuracy = evaluate.load('accuracy')
accuracy = evaluate.load('accuracy', module_type='metric')
print('description of `accuracy` metric:', accuracy.description)
print('features of `accuracy` metric:', accuracy.features)

# %% build the predictions & references iteratively and compute accuracy
for ref, pred in zip([0, 1, 0, 1], [1, 0, 0, 1]):
  accuracy.add(references=ref, predictions=pred)
print('accuracy when using add:', accuracy.compute())

for refs, preds in zip([[0, 1], [0, 1]], [[1, 0], [0, 1]]):
  accuracy.add_batch(references=refs, predictions=preds)
print('accuracy with add_batch:', accuracy.compute())

## example usage in real code:
# for model_inputs, gold_standards in evaluation_dataset:
#     predictions = model(model_inputs)
#     metric.add_batch(references=gold_standards, predictions=predictions)
# metric.compute()

# %% combining several metrics
afpr_metrics = evaluate.combine(['accuracy', 'f1', 'precision', 'recall'])
results = afpr_metrics.compute(predictions=[0, 1, 0], references=[0, 1, 1])
print('accuracy, f1, precision, recall metrics:', results)

# %% save and push evaluation results to the hub
accuracy = evaluate.load('accuracy', module_type='metric')
results = accuracy.compute(references=[0, 1, 0, 1], predictions=[1, 0, 0, 1])
hyperparams = {'model': 'bert-base-uncased'}
saved_path = evaluate.save('./results/', experiment='run 42', **results, **hyperparams)
print('saved evaluation results at:', saved_path)

# evaluate.push_to_hub(
#   model_id="huggingface/gpt2-wikitext2",  # model repository on hub
#   metric_value=0.5,                       # metric value
#   metric_type="bleu",                     # metric name, e.g. accuracy.name
#   metric_name="BLEU",                     # pretty name which is displayed
#   dataset_type="wikitext",                # dataset name on the hub
#   dataset_name="WikiText",                # pretty name
#   dataset_split="test",                   # dataset split used
#   task_type="text-generation",            # task id, see https://github.com/huggingface/datasets/blob/master/src/datasets/utils/resources/tasks.json
#   task_name="Text Generation"             # pretty name for task
# )

# %% use evaluate.evaluator()
pipe = pipeline('text-classification', model='lvwerra/distilbert-imdb', device='cpu')
data = load_dataset('imdb', split='test').shuffle().select(range(1000))
metric = evaluate.load('accuracy')

task_evaluator = evaluator('text-classification')
results = task_evaluator.compute(
  model_or_pipeline=pipe,
  data=data,
  metric=metric,
  label_mapping={'NEGATIVE': 0, 'POSITIVE': 1},
)
print('evaluator results:', results)

results = task_evaluator.compute(
  model_or_pipeline=pipe,
  data=data,
  metric=metric,
  label_mapping={'NEGATIVE': 0, 'POSITIVE': 1},
  strategy='bootstrap',
  n_resamples=200,
)
print('evaluator results with bootstrapping:', results)

# %% Visualising different metrics using radar plot
data = [
  {'accuracy': 0.99, 'precision': 0.8, 'f1': 0.95, 'latency_in_seconds': 33.6},
  {'accuracy': 0.98, 'precision': 0.87, 'f1': 0.91, 'latency_in_seconds': 11.2},
  {'accuracy': 0.98, 'precision': 0.78, 'f1': 0.88, 'latency_in_seconds': 87.6},
  {'accuracy': 0.88, 'precision': 0.78, 'f1': 0.81, 'latency_in_seconds': 101.6},
]
model_names = ['Model 1', 'Model 2', 'Model 3', 'Model 4']
plot = radar_plot(data=data, model_names=model_names)
plot.show()


# %% Running evaluation on a suit of tasks
class Suite(evaluate.EvaluationSuite):
  def __init__(self, name):
    super().__init__(name)

    self.suite = [
      SubTask(
        task_type='text-classification',
        data='imdb',
        split='test[:1]',
        args_for_task={
          'metric': 'accuracy',
          'input_column': 'text',
          'label_column': 'label',
          'label_mapping': {'NEGATIVE': 0, 'POSITIVE': 1},
        },
      ),
      SubTask(
        task_type='text-classification',
        data='sst2',
        split='test[:1]',
        args_for_task={
          'metric': 'accuracy',
          'input_column': 'sentence',
          'label_column': 'label',
          'label_mapping': {'NEGATIVE': 0, 'POSITIVE': 1},
        },
      ),
    ]


suite = Suite('two-tasks')
results = suite.run('distilbert-base-uncased-finetuned-sst-2-english')
print('results:', pd.DataFrame(results))

suite = EvaluationSuite.load('mathemakitten/sentiment-evaluation-suite')
results = suite.run('huggingface/prunebert-base-uncased-6-finepruned-w-distil-mnli')
print('results:', pd.DataFrame(results))
